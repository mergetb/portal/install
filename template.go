package main

var caCSR = `{
  "CN": "{{.Ca}}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "{{.C}}",
      "L": "{{.L}}",
      "ST": "{{.St}}",
      "O": "{{.O}}",
      "OU": "{{.Ou}}"
    }
  ]
}
`

var caConfig = `{
  "signing": {
    "default": {
      "expiry": "{{.Expiry}}h"
    },
    "profiles": {
      "mergetb": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "{{.Expiry}}h"
      }
    }
  }
}
`

//TODO set token expiration for longer
var kratosConfig = `# see : https://www.ory.sh/kratos/docs/reference/configuration

dsn: "postgres://oauth:{{ .Generated.Auth.PostgresPW }}@postgres:5432/oauth?sslmode=disable"

selfservice:
  default_browser_return_url: https://auth.{{ .Parameterized.Domain.Api }}/.login/web/
  whitelisted_return_urls:
  -  https://auth.{{ .Parameterized.Domain.Api }}/.login/web/
  -  https://launch.{{ .Parameterized.Domain.Api }}
  -  https://console.{{ .Parameterized.Domain.Api }}
  -  http://0.0.0.0:9000
  -  https://localhost:9000
  -  https://localhost
  -  /login
  flows:
    settings:
      ui_url: https://auth.{{ .Parameterized.Domain.Api }}/.login/web/settings
      lifespan: 1h
      privileged_session_max_age: 1h
      after:
        password:
          hooks:
            - hook: verify
        profile:
          hooks:
            - hook: verify
    logout:
      after:
        default_browser_return_url: https://auth.{{ .Parameterized.Domain.Api }}/.login/web
    registration:
      ui_url: https://auth.{{ .Parameterized.Domain.Api }}/.login/web/auth/registration
      lifespan: 10m
      after:
        password:
          hooks:
            - hook: session
        oidc:
          hooks:
            - hook: session
    login:
      ui_url: https://auth.{{ .Parameterized.Domain.Api }}/.login/web/auth/login
      lifespan: 1h
    verification:
      enabled: false
      ui_url: https://auth.{{ .Parameterized.Domain.Api }}/.login/web/verify
      lifespan: 1m
    recovery:
      enabled: false
      ui_url: https://auth.{{ .Parameterized.Domain.Api }}/.login/web/recovery
      lifespan: 1h
    error:
      ui_url: https://auth.{{ .Parameterized.Domain.Api }}/.login/web/kratos-error
  methods:
    profile:
      enabled: true
    link:
      enabled: true
    password:
      enabled: true

# Not using this, but krator will not start without it.
courier:
  smtp:
    connection_uri: smtps://foo:bar@my-mailserver:1234/?skip_ssl_verify=false
    from_address: wtVe@gdr.oq
  template_override_path: /conf/courier-templates

serve:
  admin:
    base_url: http://127.0.0.1:4434

  public:
    base_url: https://auth.{{ .Parameterized.Domain.Api }}/.ory/kratos/

    cors:
      enabled: true
      debug: true
      allowed_origins:
        - https://localhost
        - https://auth.{{ .Parameterized.Domain.Api }}
        - https://console.{{ .Parameterized.Domain.Api }}
        - https://launch.{{ .Parameterized.Domain.Api }}
      allowed_headers:
        - Authorization
        - Cookie
      exposed_headers:
        - Content-Type
        - Set-Cookie

log:
  level: info
  format: text

secrets:
  cookie:
    - "{{ .Generated.Auth.CookieSecret }}"

hashers:
  argon2:
    memory: 131072
    iterations: 2
    parallelism: 1
    salt_length: 16
    key_length: 16

session:
  lifespan: 1h
  cookie:
    same_site: None
    domain: {{ .Parameterized.Domain.Api }}

identity:
  default_schema_url: file:///etc/kratos/kratos.identity.traits.schema.json
`

var kratosIdentityTraitsSchema = `{
  "$id": "https://example.com/registration.schema.json",
  "$schema": "http://json-schema.org/draft-07/schema#",
  "title": "Person",
  "type": "object",
  "properties": {
    "traits": {
      "type": "object",
      "properties": {
        "email": {
          "type": "string",
          "format": "email",
          "ory.sh/kratos": {
            "credentials": {
              "password": {
                "identifier": true
              }
            }
          }
        },
        "username": {
          "type": "string",
          "pattern": "^[a-z][a-z0-9]+$",
          "ory.sh/kratos": {
            "credentials": {
              "password": {
                "identifier": true
              }
            }
          }
        },
        "admin": {
          "type": "boolean"
        }
      },
      "required": [
        "email",
        "username"
      ],
      "additionalProperties": false
    }
  }
}
`

var policy = `
#
# This is the default/reference policy for a Merge Portal.
#
project:
  public:
    create: [Any.Any]
    read:   [Any.Any]
    update: [Project.Maintainer]
    delete: [Project.Creator]

  protected:
    create: [Any.Any]
    read:   [Project.Member]
    update: [Project.Maintainer]
    delete: [Project.Creator]

  private:
    create: [Any.Any]
    read:   [Project.Creator]
    update: [Project.Creator]
    delete: [Project.Creator]

experiment:
  public:
    create: [Project.Member]
    read:   [Any.Any]
    update: [Experiment.Maintainer, Project.Maintainer]
    delete: [Experiment.Creator, Project.Maintainer]

  protected:
    create: [Project.Member]
    read:   [Project.Member]
    update: [Experiment.Maintainer, Project.Maintainer]
    delete: [Experiment.Creator, Project.Maintainer]

  private:
    create: [Project.Member]
    read:   [Experiment.Maintainer, Project.Creator]
    update: [Experiment.Maintainer, Project.Creator]
    delete: [Experiment.Creator, Project.Creator]

xdc:
  public:
    spawn:   [Project.Member]
    destroy: [Xdc.Creator, Project.Creator, Experiment.Maintainer]
    attach:  [Xdc.Creator, Project.Creator, Experiment.Maintainer]
    detach:  [Xdc.Creator, Project.Creator, Experiment.Maintainer]

identity:
  # identity actions
  read: [Organization.Maintainer, Portal.Maintainer]
  register: [Any.Any]   # aka create a new id
  unregister: [Identity.Creator, Portal.Maintainer] # aka delete an existing id
  # user account actions
  updatestate: [Portal.Maintainer, Organization.Maintainer] # update a portal user state (active, frozen, etc)
  init: [Portal.Maintainer, Organization.Maintainer] # create a new portal user

user:
  public:
    read:   [Any.Any]
    update: [User.Creator]
    delete: [User.Creator]

  protected:
    read:   [Project.Member]
    update: [User.Creator]
    delete: [User.Creator]

  private:
    read:   [User.Creator]
    update: [User.Creator]
    delete: [User.Creator]

realization:
  public:
    create:  [Experiment.Maintainer, Project.Member]
    accept:  [Realization.Creator]
    reject:  [Realization.Creator]
    release:  [Experiment.Maintainer, Project.Member, Realization.Creator]

materialization:
  public:
    create:  [Project.Member]
    destroy: [Realization.Creator, Project.Creator]

facility:
  public:
    create: [Any.Any]
    read:   [Any.Any]
    update: [Facility.Maintainer]
    delete: [Facility.Creator, Portal.Maintainer]
      # commission: [Facility.Maintainer]
      # decommission: [Facility.Maintainer]

  protected:
    create: [Any.Any]
    read:   [Facility.Maintainer, Portal.Maintainer]
    update: [Facility.Maintainer]
    delete: [Facility.Creator, Portal.Maintainer]
      # commission: [Facility.Maintainer, Portal.Maintainer]
      # decommission: [Facility.Maintainer, Portal.Maintainer]

  private:
    create: [Any.Any]
    read:   [Facility.Maintainer, Portal.Admin]
    update: [Facility.Maintainer]
    delete: [Facility.Creator, Portal.Admin]
      # commission: [Facility.Maintainer, Portal.Admin]
      # decommission: [Facility.Creator, Portal.Admin]

pool:
  public:
    create: [Any.Any]
    read: [Any.Any]
    updatefacility: [Facility.Maintainer]
    updateproject: [Pool.Creator]
    delete: [Pool.Creator]

  # The concept of modes for Pools is ok, but the code
  # does not implment it yet so we do not define it.
`
