#!/bin/bash

# Use openshift-controller-manager from this PR
#
#   - https://github.com/openshift/openshift-controller-manager/pull/153
#

set -ex

# First set openshift-controller-manager-operator to unmanaged, otherwise
# OpenShift will go all skynet on us.
oc patch clusterversion/version --type='merge' -p "$(cat <<- EOF
spec:
  overrides:
  - group: apps/v1
    kind: Deployment
    name: openshift-controller-manager-operator
    namespace: openshift-controller-manager-operator
    unmanaged: true
EOF
)"

# Update the controller-manager image to point at an image produced from the PR
# referenced above.
oc set env \
    -n openshift-controller-manager-operator \
    deployment/openshift-controller-manager-operator \
    -c openshift-controller-manager-operator \
    IMAGE=quay.io/mergetb/openshift-controller-manager:4.7-ingress-reencrypt-fix

