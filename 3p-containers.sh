#!/bin/bash

set -ex

REGISTRY=${REGISTRY:-default-route-openshift-image-registry.apps.test-cluster.redhat.com}
MERGENS=${MERGENS:-merge}
AUTHNS=${AUTHNS:-merge-auth}
CNTDIR=build/containers

mkdir -p ${CNTDIR}/{$MERGENS,$AUTHNS}

containers=( "kratos" "postgres" "user-ui" "step-ca" "etcd" "minio" )
tags=( "v0.5" "9.6" "v1.0.0" "0.15.3" "v3.4.13" "RELEASE.2020-12-26T01-35-54Z" )
registries=( "docker.io/oryd" "docker.io/library" "registry.gitlab.com/mergetb/portal" "quay.io/mergetb" "quay.io/coreos" "docker.io/minio" )
namespaces=( $AUTHNS $AUTHNS $AUTHNS $MERGENS $MERGENS $MERGENS )

for i in "${!namespaces[@]}"; do
    ns="${namespaces[$i]}"
    cn="${containers[$i]}"
    reg="${registries[$i]}"
    tag="${tags[$i]}"

    if [[ ! -f $CNTDIR/$ns/$cn.tar ]]; then
        podman pull $reg/$cn:$tag

        podman tag $reg/$cn:$tag $REGISTRY/$ns/$cn:$tag
        podman tag $reg/$cn:$tag $REGISTRY/$ns/$cn:latest

        podman save $REGISTRY/$ns/$cn:$tag -o $CNTDIR/$ns/$cn.tar

        podman push $REGISTRY/$ns/$cn:$tag || true
        podman push $REGISTRY/$ns/$cn:latest || true
    fi
done
