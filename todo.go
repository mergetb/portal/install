package main

import (
	appsv1 "k8s.io/api/apps/v1"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

func oauth() {

	service("merge-auth", "oauth", "mergeauth", []v1.ServicePort{
		{Protocol: "TCP", Port: 443, TargetPort: intstr.FromInt(4444), Name: "public"},
		{Protocol: "TCP", Port: 4445, TargetPort: intstr.FromInt(4445), Name: "admin"},
		{Protocol: "TCP", Port: 4465, TargetPort: intstr.FromInt(4465), Name: "token-user"},
	})

	replicas := new(int32)
	*replicas = 1

	deploy(
		"merge-auth",
		"oauth",
		&appsv1.Deployment{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Deployment",
				APIVersion: "apps/v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: "oauth",
			},
			Spec: appsv1.DeploymentSpec{
				Replicas: replicas, //TODO
				Selector: &metav1.LabelSelector{
					MatchLabels: map[string]string{
						"app":       "mergeauth",
						"component": "oauth",
					},
				},
				Template: v1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app":       "mergeauth",
							"component": "oauth",
						},
					},
					Spec: v1.PodSpec{
						Tolerations: []v1.Toleration{
							{
								Key:      "dedicated",
								Operator: v1.TolerationOpEqual,
								Value:    "service_worker",
								Effect:   v1.TaintEffectNoSchedule,
							},
						},
						InitContainers: []v1.Container{
							{
								Name:            "sqlmigrate",
								Image:           img("merge-auth", "hydra"),
								ImagePullPolicy: v1.PullAlways,
								Command: []string{
									"hydra",
									"migrate",
									"sql",
									"-e",
									"--yes",
								},
								Env: []v1.EnvVar{
									{
										Name:  "DSN",
										Value: "postgres://oauth:" + gconf.Auth.PostgresPW + "@postgres:5432/oauth?sslmode=disable",
									},
								},
								VolumeMounts: []v1.VolumeMount{
									{Name: "config-volume", MountPath: "/etc/hydra"},
								},
							},
						},
						Containers: []v1.Container{
							{
								Name:            "oauth",
								Image:           img("merge-auth", "hydra"),
								ImagePullPolicy: v1.PullAlways,
								Ports: []v1.ContainerPort{
									{Name: "public", ContainerPort: 4444},
									{Name: "admin", ContainerPort: 4445},
									{Name: "token-user", ContainerPort: 4465},
								},
								VolumeMounts: []v1.VolumeMount{
									{Name: "config-volume", MountPath: "/etc/hydra"},
								},
								Command: []string{
									"hydra",
									"serve",
									"all",
									"--sqa-opt-out",
								},
								Env: []v1.EnvVar{
									{
										// for running local hydra commands on the cli
										Name:  "HYDRA_URL",
										Value: img("merge-auth", "hydra"),
									},
									{
										// These should be read from hydra.cfg via --contfig,
										//but they seem to be broken
										Name:  "SECRETS_SYSTEM",
										Value: gconf.OAuth.SystemSecret,
									},
									{
										Name:  "DSN",
										Value: "postgres://oauth:" + gconf.Auth.PostgresPW + "@postgres:5432/oauth?sslmode=disable",
									},
									{
										Name:  "URLS_SELF_ISSUER",
										Value: "https://127.0.0.1:4444",
									},
									{
										Name:  "URLS_LOGIN",
										Value: "https://user." + config.Domain.Api + "/auth/login",
									},
									{
										Name:  "URLS_LOGOUT",
										Value: "https://user." + config.Domain.Api + "/auth/logout",
									},
								},
							},
						},
						Volumes: []v1.Volume{
							{
								Name: "config-volume",
								VolumeSource: v1.VolumeSource{
									ConfigMap: &v1.ConfigMapVolumeSource{
										LocalObjectReference: v1.LocalObjectReference{
											Name: "auth-oauth-cm",
										},
									},
								},
							},
						},
					},
				},
			},
		},
	)

}

var hydraConfig = `## ORY Hydra Configuration

# see https://www.ory.sh/hydra/docs/reference/configuration

log:
  level: debug
  leak_sensitive_values: true
  format: text

# having this set breaks things but doesn't tell you they are broken.
# I think it has to do with running in http instead of https
# just horrible.
#
serve:
  public:
    port: 4444
    host: 127.0.0.1
# 
#     cors:
#       enabled: true
#       allowed_origins:
#         - "*"
#       allowed_methods:
#         - TRACE
#         - PATCH
#         - TRACE
#         - POST
#         - TRACE
#       # allowed_headers:
#       #   - aute et nulla
#       # exposed_headers:
#       #   - aute velit in dolor
#       #   - et
#       allow_credentials: true
#       # options_passthrough: true
#       # max_age: 80959111
#       debug: false
#     # socket:
#     #   owner: voluptate ex Ut ad
#     #   group: fugiat laboris pariatur aliquip adipisicing
#     #   mode: 207
#     # access_log:
#     #   disable_for_health: false
# 
  admin:
    port: 4445
    host: 127.0.0.1
#     cors:
#       enabled: false
#       allowed_origins:
#         - "*"
#       allowed_methods:
#         - POST
#         - GET
#       # allowed_headers:
#       #   - Lorem est
#       #   - voluptate labore
#       # exposed_headers:
#       #   - Ut in sint laborum
#       #   - officia
#       allow_credentials: true
#       options_passthrough: true
#       # max_age: 73698706
#       # debug: false
# 
#     # socket:
#     #   owner: voluptate
#     #   group: voluptate nulla id dolore ut
#     #   mode: 16
#     # access_log:
#     #   disable_for_health: true
# 
#   # tls:
#   #   key:
#   #     base64: b3J5IGh5ZHJhIGlzIGF3ZXNvbWUK
#   #   cert:
#   #     base64: b3J5IGh5ZHJhIGlzIGF3ZXNvbWUK
#   #   allow_termination_from:
#   #     - 127.0.0.1/32
#   #     - 127.0.0.1/32
#   cookies:
#     same_site_mode: None
#     same_site_legacy_workaround: true
# 

dsn: "postgres://oauth:{{ .Generated.Auth.PostgresPW }}@postgres:5432/oauth?sslmode=disable"

webfinger:
  
  jwks:
    broadcast_keys:
      - hydra.jwt.access-token

  # oidc_discovery:
  #   client_registration_url: https://my-service.com/clients
  #   supported_claims:
  #     - email
  #     - username
  #   supported_scope:
  #     - email
  #     - whatever
  #     - read.photos
  #   userinfo_url: https://example.org/my-custom-userinfo-endpoint

oidc:
  subject_identifiers:
    enabled:
      - public
    #  - pairwise
    # pairwise:
    #   salt: "{{ .Generated.OAuth.Salt }}"
#   dynamic_client_registration:
#     default_scope:
#       - openid
#       - offline
#       - offline_access

urls:
  self:
    issuer: http://127.0.0.1:4444
    public: http://127.0.0.1:4444
  login: http://auth.{{ .Parameterized.Domain.Api }}/login
  consent: http://{{ .Parameterized.Domain.Api }}/consent
  logout: http://{{ .Parameterized.Domain.Api }}/logout
  # error: https://my-error.app/error
  # post_logout_redirect: https://my-example.app/logout-successful

strategies:
  
  scope: DEPRECATED_HIERARCHICAL_SCOPE_STRATEGY
  access_token: jwt

ttl:
  login_consent_request: 1h
  access_token: 1h
  refresh_token: "-1"
  id_token: 1h
  auth_code: 1h

oauth2:
  expose_internal_errors: true
  hashers:
    bcrypt:
      cost: 83455078
  pkce:
    enforced: true
    enforced_for_public_clients: true

secrets:
  system:
    - "{{ .Generated.OAuth.SystemSecret }}"
  # cookie:
  #   - this-is-the-primary-secret
  #   - this-is-an-old-secret
  #   - this-is-another-old-secret

# profiling: cpu
# tracing:
#   provider: jaeger
#   service_name: ORY Hydra
#   providers:
#     jaeger:
#       local_agent_address: 127.0.0.1:6831
#       propagation: jaeger
#       sampling:
#         type: const
#         value: 1
#         server_url: http://127.0.0.1:5778/sampling
#     zipkin:
#       server_url: https://eAzqrYxacZoALeDS.hgxwIAgmr5UzduB4kurZBiRKl
`
