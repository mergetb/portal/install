Merge Portal Installer
======================

Installs the Merge portal onto an OpenShift cluster.

## Tool Requirements

- `go` >= 1.15
- `makeself`

## CGO Requirements

- `device-mapper-devel`
- `btrfs-progs-devel`
- `gpgme-devel`
-
-## Kubernetes Requirements

Existing physical volumes as seen in portal.yml. If your k8s
instance does not have them, you can use the following to create them:

```
ansible-playbook -e mergens=merge-v1 -i ansible-hosts prepare-volumes.yml
```

This command will create the persistent volumes in your k8s installation.
All Merge Portal persistent volumes will be appended with the value 
you give set `mergens`to.

You will have to edit and set the ansible configuration in `ansible-hosts` 
to match your k8s installation. Also choose a value for `mergens` that does
not exist. This value must map to the `k8s.persistentvolumes` entries in the
portal.yml file used to install the Merge Portal. 

For example given the `merge-v1` value above your `k8s.persistentvolumes` stanza 
would look like:

```
k8s:
  persistentvolumes:
    etcd: etcd-pv-merge-v1
    mergefs: mergefs-pv-merge-v1
    authfs: kratos-pv-merge-v1
    gitfs: git-pv-merge-v1
    minio: minio-pv-merge-v1
    stepcafs: step-pv-merge-v1
```

Those are the persistent volumes the Merge Portal services will use.
