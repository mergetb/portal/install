package main

type ClusterInfo struct {
	K8s K8sClusterInfo
}

type K8sClusterInfo struct {
	Nodes []string
}

var cluster = ClusterInfo{}
