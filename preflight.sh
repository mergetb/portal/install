#!/bin/bash

# A pre-flight script used for development

set -x

MERGENS=${MERGENS:-merge}
AUTHNS=${AUTHNS:-merge-auth}
XDCNS=${XDCNS:-xdc}

# Create namespaces so all the following things work
oc new-project $AUTHNS
oc new-project $MERGENS
oc new-project $XDCNS

# Add SCC policy for auth pods and workspace pod
oc adm policy add-scc-to-user anyuid system:serviceaccount:$MERGENS:default
oc adm policy add-scc-to-user anyuid system:serviceaccount:$MERGENS:deployer
oc adm policy add-scc-to-user anyuid system:serviceaccount:$AUTHNS:default
oc adm policy add-scc-to-user anyuid system:serviceaccount:$AUTHNS:deployer

# this may be too wide open for the xdc controller. xdcs need CAP_SYS_CHROOT
# to run sshd. May want to create an SCC with just that capability and 
# use that instead of giving the xdc-controller privileged scc
oc adm policy add-scc-to-user anyuid system:serviceaccount:$MERGENS:xdc-controller
oc adm policy add-scc-to-user privileged system:serviceaccount:$MERGENS:xdc-controller
oc adm policy add-scc-to-user anyuid system:serviceaccount:$XDCNS:default # xdc-jump want to run as root
oc adm policy add-scc-to-user privileged system:serviceaccount:$XDCNS:default

# Create push permissions for 'dev' user
oc -n $MERGENS policy add-role-to-user registry-viewer developer
oc -n $MERGENS policy add-role-to-user registry-editor developer
oc -n $AUTHNS policy add-role-to-user registry-viewer developer
oc -n $AUTHNS policy add-role-to-user registry-editor developer
oc -n $XDCNS policy add-role-to-user registry-viewer developer
oc -n $XDCNS policy add-role-to-user registry-editor developer

# HTTP2
oc annotate ingresses.config/cluster ingress.operator.openshift.io/default-enable-http2=true

# Use modified ingress that works for reencrypt
../ingress-fix.sh

# Pull 3rd party containers
../3p-containers.sh
