module gitlab.src.mergetb.dev/mergetb/portal/k8s/installer

go 1.15

require (
	github.com/cavaliercoder/grab v2.0.0+incompatible
	github.com/containers/image/v5 v5.8.1
	github.com/novln/docker-parser v1.0.0
	github.com/sethvargo/go-password v0.2.0
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/cobra v1.1.1
	gopkg.in/yaml.v2 v2.4.0
	k8s.io/api v0.19.0
	k8s.io/apimachinery v0.19.0
	k8s.io/client-go v0.19.0
	k8s.io/utils v0.0.0-20201110183641-67b214c5f920 // indirect
)
