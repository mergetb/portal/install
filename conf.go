package main

import (
	"strings"

	"github.com/novln/docker-parser"
	"k8s.io/client-go/kubernetes"

	v1 "k8s.io/api/core/v1"
)

type Config struct {
	Cert           CertConfig
	Domain         DomainConfig
	K8s            K8SConfig
	RemoteRegistry RemoteRegistryConfig
	OverrideImages OverrideImagesConfig
}

type Namespace struct {
	Merge string
	Xdc   string
	Auth  string
}

type K8SConfig struct {
	Api               string
	User              string
	Password          string
	Kubeconfig        string
	Openshift         bool
	PersistentVolumes PersistentVolumes
	Namespace         Namespace

	client *kubernetes.Clientset
}

type PersistentVolumes struct {
	Authfs   string
	Gitfs    string
	Minio    string
	StepCAfs string
	Etcd     string
	Mergefs  string
}

type RemoteRegistryConfig struct {
	Registry RegistryConfig
}

type RegistryConfig struct {
	Address    string
	User       string
	Password   string
	Tlsverify  bool
	Tag        string
	Internal   string
	PullPolicy string
}

type OverrideImagesConfig struct {
	// Merge services
	Apiserver   string
	Commission  string
	Cred        string
	Gitserver   string
	Identity    string
	Materialize string
	Mergefs     string
	Model       string
	Pops        string
	Realize     string
	Wgsvc       string
	Xdc         string

	// Jobs
	Opsinit string

	// Xdc services
	Sshjump   string
	Wireguard string
	Xdcbase   string

	// Third Party
	Postgres string
	Kratos   string
	Userui   string
	Stepca   string
	Etcd     string
	Minio    string
}

type DomainConfig struct {
	Api, Xdc, JumpAddr string
}

type CertConfig struct {
	Ca CAConfig
}

type CAConfig struct {
	Ca, C, L, St, O, Ou string
	Expiry              uint
}

var config = Config{
	K8s: K8SConfig{
		Api:       "https://api.crc.testing:6443",
		Openshift: true,
	},
	Domain: DomainConfig{
		Api:      "mergetb.acme.net",
		Xdc:      "mergetb.acme.io",
		JumpAddr: "192.168.130.11",
	},
	RemoteRegistry: RemoteRegistryConfig{
		Registry: RegistryConfig{
			Address:    "", // do not have default address. we may not want to push.
			User:       "",
			PullPolicy: "always",
		},
	},
	Cert: CertConfig{
		Ca: CAConfig{
			Ca:     "mergetb.acme.net",
			C:      "USA",
			L:      "NYC",
			St:     "New York",
			O:      "acme",
			Ou:     "mergetb",
			Expiry: 87600,
		},
	},
}

func (config Config) GetImagePullPolicy() v1.PullPolicy {

	m := make(map[string]v1.PullPolicy)
	m["always"] = v1.PullAlways
	m["pullalways"] = v1.PullAlways

	m["never"] = v1.PullNever
	m["pullnever"] = v1.PullNever

	m["ifnotpresent"] = v1.PullIfNotPresent
	m["pullifnotpresent"] = v1.PullIfNotPresent

	if policy, ok := m[strings.ToLower(config.RemoteRegistry.Registry.PullPolicy)]; ok {
		return policy
	}

	// Default to pull always
	return v1.PullAlways

}

func (config Config) GetImageOverride(name string) string {

	m := make(map[string]string)

	// Merge services
	// m["api"] = config.OverrideImages.Apiserver
	m["apiserver"] = config.OverrideImages.Apiserver
	m["commission"] = config.OverrideImages.Commission
	m["cred"] = config.OverrideImages.Cred
	m["git-server"] = config.OverrideImages.Gitserver
	m["identity"] = config.OverrideImages.Identity
	m["materialize"] = config.OverrideImages.Materialize
	m["mergefs"] = config.OverrideImages.Mergefs
	m["model"] = config.OverrideImages.Model
	m["pops"] = config.OverrideImages.Pops
	m["realize"] = config.OverrideImages.Realize
	m["wgsvc"] = config.OverrideImages.Wgsvc
	m["xdc"] = config.OverrideImages.Xdc

	// Jobs
	m["ops-init"] = config.OverrideImages.Opsinit

	// Xdc services
	m["ssh-jump"] = config.OverrideImages.Sshjump
	m["wgd"] = config.OverrideImages.Wireguard
	m["xdc-base"] = config.OverrideImages.Xdcbase

	// Third Party
	m["postgres"] = config.OverrideImages.Postgres
	m["kratos"] = config.OverrideImages.Kratos
	m["step-ca"] = config.OverrideImages.Stepca
	m["user-ui"] = config.OverrideImages.Userui
	m["etcd"] = config.OverrideImages.Etcd
	m["minio"] = config.OverrideImages.Minio

	image := m[strings.ToLower(name)]

	// Check if it has a repo and tag, and if not, add the defaults
	if image != "" {
		ref, err := dockerparser.Parse(image)

		if err != nil {
			return image
		}

		// Check if it had a registry
		if !strings.HasPrefix(image, ref.Registry()) {
			image = config.RemoteRegistry.Registry.Internal + "/" + image
		}

		// Check if it had a tag
		if !strings.HasSuffix(image, ref.Tag()) {
			image += ":" + config.RemoteRegistry.Registry.Tag
		}
	}

	return image
}
