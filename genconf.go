package main

type GenConf struct {
	Auth  AuthConf
	OAuth OAuthConf
	Merge MergeConf
	Minio MinIOConf
}

type MinIOConf struct {
	Access string
	Secret string
}

type AuthConf struct {
	CookieSecret string
	PostgresPW   string
	StepCAPW     string
}

type OAuthConf struct {
	Salt         string
	SystemSecret string
}

type MergeConf struct {
	OpsPW string
}

var gconf GenConf
