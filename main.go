package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"

	cp "github.com/containers/image/v5/copy"
	"github.com/containers/image/v5/docker"
	"github.com/containers/image/v5/docker/archive"
	"github.com/containers/image/v5/openshift"
	"github.com/containers/image/v5/types"

	// // register openshift transport
	// _ "github.com/containers/image/v5/openshift"
	"github.com/containers/image/v5/signature"

	"github.com/sethvargo/go-password/password"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"

	//netv1beta1 "k8s.io/api/networking/v1beta1"
	netv1 "k8s.io/api/networking/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

var (
	configfile string
	force      bool
	redeploy   bool
	mergeNs    string
	xdcNs      string
	authNs     string
)

func main() {

	log.SetFormatter(&log.TextFormatter{
		DisableTimestamp: true,
	})

	root := &cobra.Command{
		Use:   "portal-install",
		Short: "Merge Portal Installer",
		Run:   func(cmd *cobra.Command, args []string) { install() },
	}
	root.PersistentFlags().StringVarP(
		&configfile, "config", "c", "portal.config", "Installation config file")
	root.PersistentFlags().BoolVarP(
		&force, "force", "f", false, "Overwrite any existing config")
	root.PersistentFlags().BoolVarP(
		&redeploy, "redeploy", "d", false, "Overwrite any existing deployment")

	root.Execute()
}

func install() {

	log.Info("Beginning Merge Portal installation")

	if force {
		log.Warn("clearing out old config")
		err := os.RemoveAll(".cert")
		if err != nil {
			log.Fatal(err)
		}
	}

	configure()
	genconf()
	collect()
	tools()
	certs()
	namespaces()
	serviceAccounts()
	configmaps()
	push()
	apps()
	daamonsets()
	ingresses()
	jobs()
}

func configure() {

	defer func() {
		ydump(config, "Your config\n%s", log.Infof)
		checkConfig()
	}()

	if !fileExists(configfile) {
		return
	}

	buf, err := ioutil.ReadFile(configfile)
	if err != nil {
		log.Fatal(err)
	}

	err = yaml.Unmarshal(buf, &config)
	if err != nil {
		log.Fatal(err)
	}

	// set global namespaces for ease of reference.
	mergeNs = config.K8s.Namespace.Merge
	xdcNs = config.K8s.Namespace.Xdc
	authNs = config.K8s.Namespace.Auth
}

func genconf() {

	defer func() {
		ydump(gconf, "Generated config\n%s", log.Infof)
		checkConfig()
	}()

	filename := ".conf/generated.yml"

	if fileExists(filename) {
		log.Info("Using existing generated config")
		in, err := ioutil.ReadFile(filename)
		if err != nil {
			log.Fatal(err)
		}
		err = yaml.Unmarshal(in, &gconf)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	err := os.MkdirAll(".conf", 0755)
	if err != nil {
		log.Fatal(err)
	}

	gconf.Auth.CookieSecret, err = password.Generate(32, 10, 0, false, false)
	if err != nil {
		log.Fatal(err)
	}

	gconf.Auth.PostgresPW, err = password.Generate(32, 10, 0, false, false)
	if err != nil {
		log.Fatal(err)
	}

	gconf.Auth.StepCAPW, err = password.Generate(32, 10, 0, false, false)
	if err != nil {
		log.Fatal(err)
	}

	gconf.OAuth.Salt, err = password.Generate(32, 10, 0, false, false)
	if err != nil {
		log.Fatal(err)
	}

	gconf.OAuth.SystemSecret, err = password.Generate(32, 10, 0, false, false)
	if err != nil {
		log.Fatal(err)
	}

	gconf.Merge.OpsPW, err = password.Generate(32, 10, 0, false, false)
	if err != nil {
		log.Fatal(err)
	}

	gconf.Minio.Access, err = password.Generate(32, 10, 0, false, false)
	if err != nil {
		log.Fatal(err)
	}

	gconf.Minio.Secret, err = password.Generate(32, 10, 0, false, false)
	if err != nil {
		log.Fatal(err)
	}

	out, err := yaml.Marshal(gconf)
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile(filename, out, 0644)
	if err != nil {
		log.Fatal(err)
	}
}

func checkConfig() {

	log.Info("checking config")

	if config.K8s.Kubeconfig == "" {
		config.K8s.Kubeconfig = "/opt/k8s/kubeconfig"
		ydump(
			struct{ K8s K8SConfig }{K8s: config.K8s},
			"could not find kubeconfig, supply explicitly (via --config), example:\n%s",
			log.Fatalf,
		)
	}

}

func collect() {

	k8config, err := clientcmd.BuildConfigFromFlags("", config.K8s.Kubeconfig)
	if err != nil {
		log.Fatalf("failed to create k8s config: %v", err)
	}

	config.K8s.client, err = kubernetes.NewForConfig(k8config)
	if err != nil {
		log.Fatalf("failed to create k8s client: %v", err)
	}

	nodes, err := config.K8s.client.CoreV1().Nodes().List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		log.Fatalf("failed to list nodes: %v", err)
	}

	for _, node := range nodes.Items {
		cluster.K8s.Nodes = append(cluster.K8s.Nodes, node.Name)
	}

	ydump(cluster, "Your cluster\n%s", log.Infof)

}

func tools() {

	require("cfssl", "https://pkg.cfssl.org/R1.2/cfssl_linux-amd64")
	require("cfssljson", "https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64")

}

func certs() {

	err := os.MkdirAll(".cert", 0755)
	if err != nil {
		log.Fatal(err)
	}

	genca()
	gencert("apiserver", "api."+config.Domain.Api, "grpc."+config.Domain.Api, "apiserver")
	gencert("auth", config.Domain.Api, fqdn("auth", config.Domain.Api))
	gencert("api", config.Domain.Api, "api", fqdn("api", config.Domain.Api))
	gencert("admin", config.Domain.Api, "admin", fqdn("admin", config.Domain.Api))
	gencert("cred", config.Domain.Api, "cred") // credential managment.
	gencert("materialize", config.Domain.Api, "materialize")
	gencert("wgsvc", config.Domain.Api, "wgsvc")
	gencert("etcd", config.Domain.Api, "etcd")
	gencert("git", config.Domain.Api, "git", fqdn("git", config.Domain.Api))
	gencert("mergfs", config.Domain.Api, "mergfs") // credential managment.
	gencert("minio", config.Domain.Api, "minio")
	gencert("stepca", config.Domain.Api, "stepca") // CA for cred managment.
	gencert("wgdsvc", config.Domain.Api, "wgdsvc")
	gencert("wgd", config.Domain.Api, cluster.K8s.Nodes...)
	gencert("xdc", config.Domain.Xdc, config.Domain.Xdc)

	// not used directly, but used by spawned XDCs
	gencert("xdcingress", fqdn("xdc", config.Domain.Xdc), fqdn("*.xdc", config.Domain.Xdc))
}

func namespaces() {

	log.Info("Creating namespaces")
	namespace(mergeNs)
	namespace(authNs)
	namespace(xdcNs)

}

func serviceAccounts() {

	log.Info("Creating service accounts")

	serviceAccount(mergeNs, "xdc-controller")

	// XDC PVC Control
	clusterRole("xdc-pvc-controller", []rbacv1.PolicyRule{{
		APIGroups: []string{""},
		Resources: []string{"persistentvolumeclaims"},
		Verbs:     []string{"get", "list", "create", "delete"},
	}})
	clusterRoleBinding("xdc-pvc-controller-binding", xdcNs,
		[]rbacv1.Subject{{
			Kind:      rbacv1.ServiceAccountKind,
			Name:      "xdc-controller",
			Namespace: mergeNs,
		}},
		rbacv1.RoleRef{
			APIGroup: rbacv1.GroupName,
			Kind:     "ClusterRole",
			Name:     "xdc-pvc-controller",
		},
	)

	// XDC Deployment Control
	role("xdc-deployment-controller", xdcNs, []rbacv1.PolicyRule{
		{
			APIGroups: []string{"extensions", "apps"},
			Resources: []string{"deployments"},
			Verbs:     []string{"get", "list", "create", "delete", "deletecollection"},
		},
		{
			APIGroups: []string{""},
			Resources: []string{"pods", "services", "configmaps"},
			Verbs:     []string{"get", "list", "create", "delete"},
		},
		{
			APIGroups: []string{"networking.k8s.io"},
			Resources: []string{"ingresses"},
			Verbs:     []string{"get", "list", "create", "delete"},
		},
	})
	roleBinding("xdc-deployment-controller-binding", xdcNs,
		[]rbacv1.Subject{{
			Kind:      rbacv1.ServiceAccountKind,
			Name:      "xdc-controller",
			Namespace: mergeNs,
		}},
		rbacv1.RoleRef{
			APIGroup: rbacv1.GroupName,
			Kind:     "Role",
			Name:     "xdc-deployment-controller",
		},
	)

	// XDC Pod reader. Assumes the merge namespace exists.
	role("pod-reader", xdcNs, []rbacv1.PolicyRule{{
		APIGroups: []string{""},
		Resources: []string{"pods"},
		Verbs:     []string{"get", "list", "watch"},
	}})
	roleBinding("xdc-pod-reader", xdcNs,
		[]rbacv1.Subject{{
			Kind:      rbacv1.ServiceAccountKind,
			Name:      "xdc-controller",
			Namespace: mergeNs,
		}, {
			Kind:      rbacv1.ServiceAccountKind,
			Name:      "default", // other non--xdc-operator pds watch XDCs as well.
			Namespace: mergeNs,
		}},
		rbacv1.RoleRef{
			APIGroup: "",
			Kind:     "Role",
			Name:     "pod-reader",
		},
	)
}

func configmaps() {

	log.Info("Creating configmaps")
	certCM(mergeNs, "api")
	certCM(mergeNs, "etcd")
	certCM(mergeNs, "minio")
	certCM(mergeNs, "wgdsvc")
	certCM(mergeNs, "wgd")
	certCM(mergeNs, "apiserver")
	certCM(mergeNs, "stepca")
	certCM(mergeNs, "cred")
	certCM(mergeNs, "xdc")
	certCM(mergeNs, "mergfs")
	certCM(mergeNs, "materialize")
	certCM(xdcNs, "xdcingress")

	confdata := struct {
		Generated     GenConf
		Parameterized Config
	}{
		Generated:     gconf,
		Parameterized: config,
	}

	mapCM(authNs, "auth", map[string]string{
		"kratos.yml":                         templateString(kratosConfig, confdata),
		"kratos.identity.traits.schema.json": templateString(kratosIdentityTraitsSchema, gconf.Auth),
	}, nil)

	mapCM(mergeNs, "policy", map[string]string{
		"policy.yml": templateString(policy, nil),
	}, nil)

	fileSecrets(mergeNs, "gitkey", map[string]string{
		"tls.crt": ".cert/git.pem",
		"tls.key": ".cert/git-key.pem",
	})

	fileSecrets(authNs, "apikey", map[string]string{
		"tls.crt": ".cert/api.pem",
		"tls.key": ".cert/api-key.pem",
	})

	fileSecrets(authNs, "authkey", map[string]string{
		"tls.crt": ".cert/auth.pem",
		"tls.key": ".cert/auth-key.pem",
	})

	fileSecrets(mergeNs, "apikey", map[string]string{
		"tls.crt": ".cert/api.pem",
		"tls.key": ".cert/api-key.pem",
	})

	fileSecrets(xdcNs, "apikey", map[string]string{
		"tls.crt": ".cert/api.pem",
		"tls.key": ".cert/api-key.pem",
	})

	fileSecrets(mergeNs, "apiserverkey", map[string]string{
		"tls.crt":    ".cert/apiserver.pem",
		"tls.key":    ".cert/apiserver-key.pem",
		"ca.crt":     ".cert/ca.pem",
		"cacert.crt": ".cert/ca.pem",
	})

	fileSecrets(mergeNs, "stepcakey", map[string]string{
		"tls.crt": ".cert/stepca.pem",
		"tls.key": ".cert/stepca-key.pem",
	})

	fileSecrets(xdcNs, "xdcingresskey", map[string]string{
		"tls.crt": ".cert/xdcingress.pem",
		"tls.key": ".cert/xdcingress-key.pem",
	})
}

func push() {

	if config.RemoteRegistry.Registry.Address == "" {
		log.Info("No remote registry. Not pushing containers.")
		return
	}

	log.Info("Pushing containers")

	pushContainer(mergeNs, "apiserver")
	pushContainer(mergeNs, "identity")
	pushContainer(mergeNs, "ops-init")
	pushContainer(mergeNs, "git-server")
	pushContainer(mergeNs, "model")
	pushContainer(mergeNs, "realize")
	pushContainer(mergeNs, "materialize")
	pushContainer(mergeNs, "cred")
	pushContainer(mergeNs, "xdc")
	pushContainer(mergeNs, "step-ca")
	pushContainer(mergeNs, "mergefs")
	pushContainer(mergeNs, "wgsvc")
	pushContainer(mergeNs, "pops")
	pushContainer(mergeNs, "commission")
	pushContainer(mergeNs, "etcd")
	pushContainer(mergeNs, "minio")

	pushContainer(xdcNs, "wgd")
	pushContainer(xdcNs, "xdc-base")
	pushContainer(xdcNs, "ssh-jump")

	pushContainer(authNs, "kratos")
	pushContainer(authNs, "postgres")
	pushContainer(authNs, "user-ui")
}

func pushContainer(project, name string) {

	log.Infof("Pushing %s/%s", project, name)

	source, err := archive.Transport.ParseReference(
		fmt.Sprintf("containers/%s/%s.tar", project, name),
	)
	if err != nil {
		log.Fatalf("parse reference: %v", err)
	}

	dest, err := openshift.ParseReference(
		fmt.Sprintf(
			"%s/%s/%s:%s",
			config.RemoteRegistry.Registry.Address,
			project,
			name,
			config.RemoteRegistry.Registry.Tag,
		),
	)
	if err != nil {
		log.Fatalf("parse image name: %v", err)
	}

	skipVerify := types.OptionalBoolFalse
	if !config.RemoteRegistry.Registry.Tlsverify {
		skipVerify = types.OptionalBoolTrue
	}

	sysctx := &types.SystemContext{
		DockerInsecureSkipTLSVerify: skipVerify,
		DockerAuthConfig: &types.DockerAuthConfig{
			Username: config.RemoteRegistry.Registry.User,
			Password: config.RemoteRegistry.Registry.Password,
		},
	}

	err = docker.CheckAuth(context.TODO(), sysctx,
		config.RemoteRegistry.Registry.User,
		config.RemoteRegistry.Registry.Password,
		config.RemoteRegistry.Registry.Address,
	)
	if err != nil {
		log.Fatalf("check auth: %v", err)
	}

	policy, err := signature.DefaultPolicy(sysctx)
	if err != nil {
		log.Fatalf("default policy: %v", err)
	}

	pctx, err := signature.NewPolicyContext(policy)
	if err != nil {
		log.Fatalf("policy context: %v", err)
	}

	opts := &cp.Options{
		DestinationCtx:     sysctx,
		ImageListSelection: cp.CopyAllImages,
	}
	_, err = cp.Image(context.TODO(), pctx, dest, source, opts)
	if err != nil {
		log.Fatalf("failed to push image: %v", err)
	}

}

func apps() {

	log.Info("creating deployments")

	// Create the pv & pvcs that will be used by spawned XDCs.
	// We duplicate the PVs as the XDCs run in a different k8s namespace
	// and this is how to mount pvs cross-namespace.
	// NOTE: pvs are not namespaced so they need to be differently named based
	// on configured namespace of services. pvcs are namespaced, so they
	// do not need difference names.
	duppv("pv-mergefs-"+xdcNs, config.K8s.PersistentVolumes.Mergefs)
	pvc(xdcNs, "pvc-mergefs-xdc", "pv-mergefs-"+xdcNs, "100Gi")

	duppv("pv-step-ca-"+xdcNs, config.K8s.PersistentVolumes.StepCAfs)
	pvc(xdcNs, "pvc-step-ca-xdc", "pv-step-ca-"+xdcNs, "5Gi")

	etcdStorage()
	minioStorage()
	stepca()

	apiService("apiserver")
	basicService(
		mergeNs,
		"identity",
		6000,
		[]v1.EnvVar{
			{Name: "AUTH_NAMESPACE", Value: config.K8s.Namespace.Auth},
		},
	)

	basicWorker(
		"wgsvc",
		[]v1.EnvVar{
			{Name: "MINIO_ACCESS_KEY", Value: gconf.Minio.Access},
			{Name: "MINIO_SECRET_KEY", Value: gconf.Minio.Secret},
			{Name: "XDC_NAMESPACE", Value: xdcNs}, // need to talk to xdcd on xdcs.
		},
		nil,
	)
	mergefsService()
	xdcService()
	credService()
	gitServer()
	basicWorker(
		"materialize",
		[]v1.EnvVar{
			{Name: "MINIO_ACCESS_KEY", Value: gconf.Minio.Access},
			{Name: "MINIO_SECRET_KEY", Value: gconf.Minio.Secret},
		},
		nil,
	)
	// model get s simple service and a deployment.
	service(mergeNs, "model", "portal", []v1.ServicePort{{
		Protocol:   "TCP",
		Port:       6000,
		TargetPort: intstr.FromInt(int(6000)),
	}})
	basicWorker(
		"model",
		[]v1.EnvVar{
			{Name: "MINIO_ACCESS_KEY", Value: gconf.Minio.Access},
			{Name: "MINIO_SECRET_KEY", Value: gconf.Minio.Secret},
		},
		nil,
	)

	basicWorker(
		"realize",
		[]v1.EnvVar{
			{Name: "MINIO_ACCESS_KEY", Value: gconf.Minio.Access},
			{Name: "MINIO_SECRET_KEY", Value: gconf.Minio.Secret},
		},
		nil,
	)
	basicWorker(
		"commission",
		[]v1.EnvVar{
			{Name: "MINIO_ACCESS_KEY", Value: gconf.Minio.Access},
			{Name: "MINIO_SECRET_KEY", Value: gconf.Minio.Secret},
		},
		nil,
	)
	basicWorker(
		"pops",
		[]v1.EnvVar{
			{Name: "MINIO_ACCESS_KEY", Value: gconf.Minio.Access},
			{Name: "MINIO_SECRET_KEY", Value: gconf.Minio.Secret},
			{Name: "ETCDCTL_API", Value: "3"},
			{Name: "ETCDCTL_ENDPOINTS", Value: "etcd:2379"},
			{Name: "ETCDCTL_CACERT", Value: "/dbcerts/ca.pem"},
			{Name: "ETCDCTL_CERT", Value: "/dbcerts/etcd.pem"},
			{Name: "ETCDCTL_KEY", Value: "/dbcerts/etcd-key.pem"},
		},
		nil,
	)

	xdcSshJump()
	authentication()

}

func authentication() {

	postgres()
	auth()

}

func api() {

	service(mergeNs, "api", "portal", []v1.ServicePort{{
		Protocol:   "TCP",
		Port:       4433,
		TargetPort: intstr.FromInt(4433),
	}})

	deployment(
		mergeNs,
		"api",
		img(mergeNs, "api"),
		[]v1.ContainerPort{
			{Name: "grpc", ContainerPort: 4433},
		},
		[]v1.VolumeMount{
			{
				Name:      "db-cert-volume",
				MountPath: "/dbcerts",
			},
			{
				Name:      "policy-volume",
				MountPath: "/etc/merge/policy.yml",
				SubPath:   "policy.yml",
			},
		},
		[]v1.Volume{
			{
				Name: "db-cert-volume",
				VolumeSource: v1.VolumeSource{
					ConfigMap: &v1.ConfigMapVolumeSource{
						LocalObjectReference: v1.LocalObjectReference{
							Name: "etcd",
						},
					},
				},
			},
			{
				Name: "policy-volume",
				VolumeSource: v1.VolumeSource{
					ConfigMap: &v1.ConfigMapVolumeSource{
						LocalObjectReference: v1.LocalObjectReference{
							Name: "policy",
						},
					},
				},
			},
		},
		nil,
		nil,
	)

}

func postgres() {
	pvc(authNs, "auth", config.K8s.PersistentVolumes.Authfs, "5Gi")

	service(authNs, "postgres", "mergeauth", []v1.ServicePort{
		{Protocol: "TCP", Port: 5432, TargetPort: intstr.FromInt(5432), Name: "db"},
	})

	replicas := new(int32)
	*replicas = 1

	deploy(
		authNs,
		"postgres",
		&appsv1.Deployment{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Deployment",
				APIVersion: "apps/v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: "postgres",
			},
			Spec: appsv1.DeploymentSpec{
				Replicas: replicas, //TODO
				Selector: &metav1.LabelSelector{
					MatchLabels: map[string]string{
						"app":       "mergeauth",
						"component": "postgres",
					},
				},
				Template: v1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app":       "mergeauth",
							"component": "postgres",
						},
					},
					Spec: v1.PodSpec{
						Tolerations: []v1.Toleration{
							{
								Key:      "dedicated",
								Operator: v1.TolerationOpEqual,
								Value:    "service_worker",
								Effect:   v1.TaintEffectNoSchedule,
							},
						},
						Containers: []v1.Container{
							{
								Name:            "postgres",
								Image:           img(authNs, "postgres"),
								ImagePullPolicy: config.GetImagePullPolicy(),
								Ports: []v1.ContainerPort{
									{Name: "public", ContainerPort: 5432},
								},
								VolumeMounts: []v1.VolumeMount{
									{Name: "data-volume", MountPath: "/var/lib/postgresql/data"},
								},
								Env: []v1.EnvVar{
									{
										Name:  "POSTGRES_USER",
										Value: "oauth",
									},
									{
										Name:  "POSTGRES_PASSWORD",
										Value: gconf.Auth.PostgresPW,
									},
								},
							},
						},
						Volumes: []v1.Volume{
							{
								Name: "data-volume",
								VolumeSource: v1.VolumeSource{
									PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
										ClaimName: "auth",
									},
								},
							},
						},
					},
				},
			},
		},
	)
}

func auth() {

	service(authNs, "auth", "mergeauth", []v1.ServicePort{
		{Protocol: "TCP", Port: 4455, TargetPort: intstr.FromInt(4455), Name: "web-login"},
		{Protocol: "TCP", Port: 4465, TargetPort: intstr.FromInt(4465), Name: "cli-login"},
		{Protocol: "TCP", Port: 4433, TargetPort: intstr.FromInt(4433), Name: "kratos-public"},
		{Protocol: "TCP", Port: 4434, TargetPort: intstr.FromInt(4434), Name: "kratos-admin"},
	})

	replicas := new(int32)
	*replicas = 1

	deploy(
		authNs,
		"auth",
		&appsv1.Deployment{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Deployment",
				APIVersion: "apps/v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: "auth",
			},
			Spec: appsv1.DeploymentSpec{
				Replicas: replicas, //TODO
				Selector: &metav1.LabelSelector{
					MatchLabels: map[string]string{
						"app":       "mergeauth",
						"component": "auth",
					},
				},
				Template: v1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app":       "mergeauth",
							"component": "auth",
						},
					},
					Spec: v1.PodSpec{
						Tolerations: []v1.Toleration{
							{
								Key:      "dedicated",
								Operator: v1.TolerationOpEqual,
								Value:    "service_worker",
								Effect:   v1.TaintEffectNoSchedule,
							},
						},
						InitContainers: []v1.Container{
							{
								Name:            "sqlmigrate",
								Image:           img(authNs, "kratos"),
								ImagePullPolicy: config.GetImagePullPolicy(),
								VolumeMounts: []v1.VolumeMount{
									{Name: "config-volume", MountPath: "/etc/kratos"},
								},
								Command: []string{
									"kratos",
									"migrate",
									"sql",
									"-e",
									"--yes",
									"--config",
									"/etc/kratos/kratos.yml",
								},
							},
						},
						Containers: []v1.Container{
							{
								Name:            "kratos",
								Image:           img(authNs, "kratos"),
								ImagePullPolicy: config.GetImagePullPolicy(),
								Ports: []v1.ContainerPort{
									{Name: "public", ContainerPort: 4433},
									{Name: "admin", ContainerPort: 4434},
								},
								VolumeMounts: []v1.VolumeMount{
									{Name: "config-volume", MountPath: "/etc/kratos"},
								},
								Command: []string{
									"kratos",
									"serve",
									"--config",
									"/etc/kratos/kratos.yml",
									"--disable-telemetry",
								},
							},
							{
								Name:            "web-login",
								Image:           img(authNs, "user-ui"),
								ImagePullPolicy: config.GetImagePullPolicy(),
								Env: []v1.EnvVar{
									{
										Name:  "KRATOS_PUBLIC_URL",
										Value: "http://127.0.0.1:4433",
									},
									{
										Name:  "KRATOS_ADMIN_URL",
										Value: "http://127.0.0.1:4434",
									},
									{
										Name:  "KRATOS_BROWSER_URL",
										Value: fmt.Sprintf("https://auth.%s/.ory/kratos/", config.Domain.Api),
									},
									{
										Name:  "BASE_URL",
										Value: fmt.Sprintf("https://auth.%s/.login/web/", config.Domain.Api),
									},
									{
										Name:  "PORT",
										Value: "4455",
									},
									{
										Name:  "SECURITY_MODE",
										Value: "cookie",
									},
								},
								Ports: []v1.ContainerPort{
									{Name: "api", ContainerPort: 4455},
								},
								VolumeMounts: []v1.VolumeMount{
									{Name: "config-volume", MountPath: "/etc/kratos"},
								},
							},
						},
						Volumes: []v1.Volume{
							{
								Name: "config-volume",
								VolumeSource: v1.VolumeSource{
									ConfigMap: &v1.ConfigMapVolumeSource{
										LocalObjectReference: v1.LocalObjectReference{
											Name: "auth",
										},
									},
								},
							},
						},
					},
				},
			},
		},
	)

}

func etcdStorage() {

	pvc(mergeNs, "etcd", config.K8s.PersistentVolumes.Etcd, "2Gi")

	service(mergeNs, "etcd", "portal", []v1.ServicePort{{
		Protocol:   "TCP",
		Port:       2379,
		TargetPort: intstr.FromInt(2379),
	}})

	replicas := new(int32)
	*replicas = 1

	deploy(
		mergeNs, "etcd",
		&appsv1.Deployment{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Deployment",
				APIVersion: "apps/v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: "etcd",
			},
			Spec: appsv1.DeploymentSpec{
				Replicas: replicas, //TODO
				Selector: &metav1.LabelSelector{
					MatchLabels: map[string]string{
						"app":       "portal",
						"component": "etcd",
					},
				},
				Template: v1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app":       "portal",
							"component": "etcd",
						},
					},
					Spec: v1.PodSpec{
						Tolerations: []v1.Toleration{
							{
								Key:      "dedicated",
								Operator: v1.TolerationOpEqual,
								Value:    "service_worker",
								Effect:   v1.TaintEffectNoSchedule,
							},
						},
						Containers: []v1.Container{
							{
								Name:            "etcd",
								Image:           img(mergeNs, "etcd"),
								ImagePullPolicy: config.GetImagePullPolicy(),
								Ports: []v1.ContainerPort{
									{Name: "etcd", ContainerPort: 2379},
								},
								VolumeMounts: []v1.VolumeMount{
									{Name: "cert-volume", MountPath: "/etcd/certs"},
									{Name: "data-volume", MountPath: "/etcd/data"},
								},
								Command: []string{
									"/usr/local/bin/etcd",
									"--name", "etcd",
									"--trusted-ca-file", "/etcd/certs/ca.pem",
									"--cert-file", "/etcd/certs/etcd.pem",
									"--key-file", "/etcd/certs/etcd-key.pem",
									"--peer-cert-file", "/etcd/certs/etcd.pem",
									"--peer-key-file", "/etcd/certs/etcd-key.pem",
									"--client-cert-auth",
									"--peer-client-cert-auth",

									"--initial-advertise-peer-urls", "https://localhost:2380",
									"--advertise-client-urls", "https://localhost:2379",
									"--listen-client-urls", "https://0.0.0.0:2379,http://localhost:4001",
									"--advertise-client-urls", "https://localhost:2379",
									"--initial-cluster", "etcd=https://localhost:2380",
									"--data-dir", "/etcd/data",
									"--max-txn-ops", "8192",
								},
								Env: []v1.EnvVar{
									{Name: "ETCDCTL_API", Value: "3"},
									{Name: "ETCDCTL_ENDPOINTS", Value: "localhost:4001"},
								},
							},
						},
						Volumes: []v1.Volume{
							{
								Name: "cert-volume",
								VolumeSource: v1.VolumeSource{
									ConfigMap: &v1.ConfigMapVolumeSource{
										LocalObjectReference: v1.LocalObjectReference{
											Name: "etcd",
										},
									},
								},
							},
							{
								Name: "data-volume",
								VolumeSource: v1.VolumeSource{
									PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
										ClaimName: "etcd",
									},
								},
							},
						},
					},
				},
			},
		},
	)

}

func minioStorage() {

	// TODO parameterize allocation limit
	pvc(mergeNs, "minio", config.K8s.PersistentVolumes.Minio, "100Gi")

	service(mergeNs, "minio", "portal", []v1.ServicePort{{
		Protocol:   "TCP",
		Port:       9000,
		TargetPort: intstr.FromInt(9000),
	}})

	replicas := new(int32)
	*replicas = 1

	deploy(
		mergeNs, "minio",
		&appsv1.Deployment{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Deployment",
				APIVersion: "apps/v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: "minio",
			},
			Spec: appsv1.DeploymentSpec{
				Replicas: replicas, //TODO
				Selector: &metav1.LabelSelector{
					MatchLabels: map[string]string{
						"app":       "portal",
						"component": "minio",
					},
				},
				Template: v1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app":       "portal",
							"component": "minio",
						},
					},
					Spec: v1.PodSpec{
						Tolerations: []v1.Toleration{
							{
								Key:      "dedicated",
								Operator: v1.TolerationOpEqual,
								Value:    "service_worker",
								Effect:   v1.TaintEffectNoSchedule,
							},
						},
						Containers: []v1.Container{
							{
								Name:            "minio",
								Image:           img(mergeNs, "minio"),
								ImagePullPolicy: config.GetImagePullPolicy(),
								Ports: []v1.ContainerPort{
									{Name: "minio", ContainerPort: 9000},
								},
								VolumeMounts: []v1.VolumeMount{
									{Name: "cert-volume", MountPath: "/certs"},
									{Name: "data-volume", MountPath: "/data"},
								},
								Env: []v1.EnvVar{
									{Name: "MINIO_ACCESS_KEY", Value: gconf.Minio.Access},
									{Name: "MINIO_SECRET_KEY", Value: gconf.Minio.Secret},
								},
								Command: []string{"minio", "server", "/data"},
							},
						},
						Volumes: []v1.Volume{
							{
								Name: "cert-volume",
								VolumeSource: v1.VolumeSource{
									ConfigMap: &v1.ConfigMapVolumeSource{
										LocalObjectReference: v1.LocalObjectReference{
											Name: "minio",
										},
									},
								},
							},
							{
								Name: "data-volume",
								VolumeSource: v1.VolumeSource{
									PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
										ClaimName: "minio",
									},
								},
							},
						},
					},
				},
			},
		},
	)

}

func ingresses() {

	log.Info("Creating ingresses")

	// Use NGINX routing functions by default
	ingressTLSPassthrough := nginxIngressTLSPassthrough
	multipathIngress := nginxMultipathIngress
	multipathRewrite := "/$1"
	multipathPostfix := "/(.*)"

	if config.K8s.Openshift {
		ingressTLSPassthrough = openshiftIngressTLSReencrypt
		multipathIngress = openshiftMultipathIngress
		multipathRewrite = "/"
		multipathPostfix = ""
	}

	ingressTLSTerminate(
		mergeNs,                                  // namespace
		"api",                                    // ingress name
		fmt.Sprintf("api.%s", config.Domain.Api), // ingress domain
		"apiserver",                              // service name
		"apikey",                                 // service key (configmap/secret)
		8081,                                     // port
	)

	// TODO
	// annoying that this needs to be a different ingress with a diffrent domain
	// name, but TLS termination for the k8s ingress object only supports 443
	//
	//   - https://kubernetes.io/docs/concepts/services-networking/ingress/#tls
	//
	ingressTLSPassthrough(
		mergeNs,
		"grpcapi",
		fmt.Sprintf("grpc.%s", config.Domain.Api),
		"apiserver",
		"apiserverkey",
		6000,
	)

	ingressTLSTerminate(
		mergeNs,
		"gitserver",
		fmt.Sprintf("git.%s", config.Domain.Api),
		"git-server",
		"gitkey",
		8080,
	)

	multipathIngress(
		authNs,
		"id",
		fmt.Sprintf("auth.%s", config.Domain.Api),
		"authkey",
		multipathRewrite,
		[]netv1.HTTPIngressPath{
			{
				Path:     "/.ory/kratos" + multipathPostfix,
				PathType: &prefixPathType,
				Backend: netv1.IngressBackend{
					Service: &netv1.IngressServiceBackend{
						Name: "auth",
						Port: netv1.ServiceBackendPort{
							Number: int32(4433),
						},
					},
				},
			},
			{
				Path:     "/.login/web" + multipathPostfix,
				PathType: &prefixPathType,
				Backend: netv1.IngressBackend{
					Service: &netv1.IngressServiceBackend{
						Name: "auth",
						Port: netv1.ServiceBackendPort{
							Number: int32(4455),
						},
					},
				},
			},
			{
				Path:     "/.login/cli" + multipathPostfix,
				PathType: &prefixPathType,
				Backend: netv1.IngressBackend{
					Service: &netv1.IngressServiceBackend{
						Name: "auth",
						Port: netv1.ServiceBackendPort{
							Number: int32(4465),
						},
					},
				},
			},
		},
	)

}

func credService() {

	// Cred service takes secrets on the command line
	// and needs to mount the step-ca certs read only.

	app, name, ns := "portal", "cred", mergeNs

	replicas := new(int32)
	*replicas = 1

	deploy(
		ns,
		name,
		&appsv1.Deployment{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Deployment",
				APIVersion: "apps/v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Spec: appsv1.DeploymentSpec{
				Replicas: replicas, //TODO
				Selector: &metav1.LabelSelector{
					MatchLabels: map[string]string{
						"app":       app,
						"component": name,
					},
				},
				Template: v1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app":       app,
							"component": name,
						},
					},
					Spec: v1.PodSpec{
						Tolerations: []v1.Toleration{
							{
								Key:      "dedicated",
								Operator: v1.TolerationOpEqual,
								Value:    "service_worker",
								Effect:   v1.TaintEffectNoSchedule,
							},
						},
						Containers: []v1.Container{
							{
								Name:            name,
								Image:           img(ns, name),
								ImagePullPolicy: config.GetImagePullPolicy(),
								VolumeMounts: []v1.VolumeMount{
									{
										Name:      "data-volume",
										MountPath: "/etc/step-ca/data/certs",
										SubPath:   "certs",
										ReadOnly:  true,
									},
									{
										Name:      "db-cert-volume",
										MountPath: "/dbcerts",
									},
								},
								Command: []string{
									"/usr/bin/cred",
									"-ca-endpoint", "https://step-ca",
									"-ca-certs-dir", "/etc/step-ca/data/certs", // should match as above
									"-cert-provider", "merge@" + config.Domain.Api,
									"-cert-prov-pw", gconf.Auth.StepCAPW,
								},
								Env: []v1.EnvVar{
									{Name: "XDC_NAMESPACE", Value: xdcNs},
								},
							},
						},
						Volumes: []v1.Volume{
							{
								Name: "data-volume",
								VolumeSource: v1.VolumeSource{
									PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
										ClaimName: "step-ca",
									},
								},
							},
							{
								Name: "db-cert-volume",
								VolumeSource: v1.VolumeSource{
									ConfigMap: &v1.ConfigMapVolumeSource{
										LocalObjectReference: v1.LocalObjectReference{
											Name: "etcd",
										},
									},
								},
							},
						},
					},
				},
			},
		},
	)

}

func stepca() {

	app, name, ns := "portal", "step-ca", mergeNs

	pvc(ns, name, config.K8s.PersistentVolumes.StepCAfs, "5Gi")

	initcmd := "step ca init -ssh --name mergeca --dns " + name + " --address 0.0.0.0:443 --provisioner merge@" + config.Domain.Api + " --provisioner-password-file /etc/step-ca/data/capass.txt --password-file /etc/step-ca/data/capass.txt"

	service(ns, name, app, []v1.ServicePort{{
		Protocol:   "TCP",
		Port:       443,
		TargetPort: intstr.FromInt(443),
		Name:       name,
	}})

	replicas := new(int32)
	*replicas = 1

	deploy(
		ns,
		name,
		&appsv1.Deployment{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Deployment",
				APIVersion: "apps/v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Spec: appsv1.DeploymentSpec{
				Replicas: replicas, //TODO
				Selector: &metav1.LabelSelector{
					MatchLabels: map[string]string{
						"app":       app,
						"component": name,
					},
				},
				Template: v1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app":       app,
							"component": name,
						},
					},
					Spec: v1.PodSpec{
						Tolerations: []v1.Toleration{
							{
								Key:      "dedicated",
								Operator: v1.TolerationOpEqual,
								Value:    "service_worker",
								Effect:   v1.TaintEffectNoSchedule,
							},
						},
						InitContainers: []v1.Container{
							{
								Name:            "ca-pass",
								Image:           img(ns, name),
								ImagePullPolicy: config.GetImagePullPolicy(),
								Command: []string{
									"/bin/sh",
									"-c",
									"echo " + gconf.Auth.StepCAPW + " > /etc/step-ca/data/capass.txt",
								},
								VolumeMounts: []v1.VolumeMount{
									{Name: "data-volume", MountPath: "/etc/step-ca/data"},
								},
							},
							{
								Name:            "ca-init",
								Image:           img(ns, name),
								ImagePullPolicy: config.GetImagePullPolicy(),
								Env: []v1.EnvVar{
									{Name: "STEPPATH", Value: "/etc/step-ca/data"},
								},
								Command: []string{
									"sh",
									"-c",
									"if [[ ! -e /etc/step-ca/data/config ]]; then " + initcmd + "; fi",
								},
								VolumeMounts: []v1.VolumeMount{
									{Name: "data-volume", MountPath: "/etc/step-ca/data"},
								},
							},
						},
						Containers: []v1.Container{
							{
								Name:            name,
								Image:           img(ns, name),
								ImagePullPolicy: config.GetImagePullPolicy(),
								Ports: []v1.ContainerPort{
									{Name: "public", ContainerPort: 443},
								},
								VolumeMounts: []v1.VolumeMount{
									{Name: "data-volume", MountPath: "/etc/step-ca/data"},
								},
								Command: []string{
									"/usr/bin/step-ca",
									"--password-file", "/etc/step-ca/data/capass.txt",
									"/etc/step-ca/data/config/ca.json",
								},
								Env: []v1.EnvVar{
									{Name: "STEPPATH", Value: "/etc/step-ca/data"},
								},
							},
						},
						Volumes: []v1.Volume{
							{
								Name: "data-volume",
								VolumeSource: v1.VolumeSource{
									PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
										ClaimName: "step-ca",
									},
								},
							},
						},
					},
				},
			},
		},
	)
}

func jobs() {

	log.Info("Creating initialization jobs")

	job(mergeNs, "ops-init", img(mergeNs, "ops-init"),
		"ops-init",
		fmt.Sprintf("ops@%s", config.Domain.Api),
		gconf.Merge.OpsPW,
	)

}

func daamonsets() {

	log.Infof("creating daemon sets")
	wgdDS()
}

func wgdDS() {

	log.Infof("creating wireguard daemon daemon set")
	name, ns := "wgd", xdcNs

	_, err := config.K8s.client.AppsV1().DaemonSets(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil {
		log.Infof("wgd daemon set already exists, skipping")
		return
	}

	priv := true
	var sockType = v1.HostPathSocket
	var dirType = v1.HostPathDirectory
	var mountPropMode = v1.MountPropagationBidirectional

	_, err = config.K8s.client.AppsV1().DaemonSets(ns).Create(
		context.TODO(),
		&appsv1.DaemonSet{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Spec: appsv1.DaemonSetSpec{
				Selector: &metav1.LabelSelector{
					MatchLabels: map[string]string{
						"app":       "portal",
						"component": name,
					},
				},
				Template: v1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app":       "portal",
							"component": name,
						},
					},
					Spec: v1.PodSpec{
						Tolerations: []v1.Toleration{{
							Key:      "dedicated",
							Operator: v1.TolerationOpEqual,
							Value:    "xdc_worker",
							Effect:   v1.TaintEffectNoSchedule,
						}},
						HostNetwork: true,
						Containers: []v1.Container{{
							Name:            name,
							Image:           img(xdcNs, "wgd"),
							ImagePullPolicy: config.GetImagePullPolicy(),
							Ports: []v1.ContainerPort{
								{Name: "grpc", ContainerPort: 36000, HostPort: 36000},
							},
							// Note that WGD needs access to this socket on the host to be able to
							// use the crictl binary to map a container ID to the netns of xdcs.
							VolumeMounts: []v1.VolumeMount{{
								Name: "crio-sock", MountPath: "/var/run/crio/crio.sock",
							}, {
								Name:             "netns-vol",
								MountPath:        "/var/run/netns",
								MountPropagation: &mountPropMode,
							}},
							Env: []v1.EnvVar{{
								Name:  "CONTAINER_RUNTIME_ENDPOINT",
								Value: "unix:///var/run/crio/crio.sock",
							}},
							SecurityContext: &v1.SecurityContext{
								Privileged: &priv,
								Capabilities: &v1.Capabilities{
									Add: []v1.Capability{
										"NET_ADMIN",
									},
								},
								SELinuxOptions: &v1.SELinuxOptions{
									Level: "s0",
								},
							},
						}},
						Volumes: []v1.Volume{{
							Name: "crio-sock",
							VolumeSource: v1.VolumeSource{
								HostPath: &v1.HostPathVolumeSource{
									Path: "/var/run/crio/crio.sock",
									Type: &sockType,
								},
							},
						}, {
							Name: "netns-vol",
							VolumeSource: v1.VolumeSource{
								HostPath: &v1.HostPathVolumeSource{
									Path: "/var/run/netns",
									Type: &dirType,
								},
							},
						}},
					},
				},
			},
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatalf("create wgd daemon set: %s", err)
	}
}
