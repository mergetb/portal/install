#!/bin/bash

set -e

SVCPATH=${SVCPATH:-mergetb/shadow/portal/services}
MLPATH=${MLPATH:-mergetb/shadow/portal/merge-login}
UIPATH=${UIPATH:-mergetb/shadow/portal/user-ui}

SVCTAG=${SVCTAG:-b0d01d35}
MLTAG=${MLTAG:-c5d2f2fb}
UITAG=${UITAG:-171eb209}

PUBREG=${PUBREG}
PUBORG=${PUBORG}
PUBTAG=${PUBTAG:-latest}
repo=

REGISTRY_USER=${REGISTRY_USER:-ci_access}

craft () {
    check_args
    prepare
    build
    collect
    package
}

check_args () {
    if [[ -z $PUBREG ]]; then
        if [[ -z $SVCPULL_SECRET ]]; then
            die "Must define environment variable SVCPULL_SECRET to access portal service container artifacts"
        fi
        if [[ -z $MLPULL_SECRET ]]; then
            die "Must define environment variable MLPULL_SECRET to access merge-login container artifacts"
        fi
        if [[ -z $UIPULL_SECRET ]]; then
            die "Must define environment variable UIPULL_SECRET to access merge-login container artifacts"
        fi
        repo=registry.gitlab.com
    else
        if [[ -z $PUBORG ]]; then
            die "Must define PUBORG if using PUBREG"
        fi
        
        # if using a public registry, path out the containers by: REG/ORG/X:TAG
        # this assumes all containers are well named, exist, and live in the given registry
        repo=$PUBREG
        SVCPATH=$PUBORG
        SVCTAG=$PUBTAG
        MLPATH=$PUBORG
        MLTAG=$PUBTAG
        UIPATH=$PUBORG
        UITAG=$PUBTAG
    fi
}

prepare () {
    mkdir -p build
}

build () {
    stage "Build installer"

    go build
    cp installer build/
}

collect () {
    stage "Collecting artifacts"

    mkdir -p build/containers/merge
    mkdir -p build/containers/merge-auth

    if [[ -z $PUBREG ]]; then # use gitlab 
        podman login -u $REGISTRY_USER -p $SVCPULL_SECRET $repo
    fi

    echo -e Collecting containers from $GREEN$repo$NORMAL 

    pull $repo $SVCPATH $SVCTAG merge \
        realize \
        materialize \
        api \
        model \
        alloc \
        admin \
        admingw \
        commission \
        xdc \
        workspace \
        discover \
        ma

    if [[ -z $PUBREG ]]; then 
        podman login -u $REGISTRY_USER -p $MLPULL_SECRET $repo
    fi

    pull $repo $MLPATH $MLTAG merge-auth merge-login

    if [[ -z $PUBREG ]]; then 
        podman login -u $REGISTRY_USER -p $UIPULL_SECRET $repo
    fi 

    pull $repo $UIPATH $UITAG merge-auth user-ui

    pull docker.io library 9.6 merge-auth postgres
    pull docker.io oryd v0.5.4 merge-auth kratos
}

pull () {
    for x in ${@:5}; do
        dopull $1 $2 $3 $4 $x &
    done
    wait
}

dopull () {
    if [[ ! -f build/containers/$4/$5.tar ]]; then
        podman pull $1/$2/$5:$3
        podman save $1/$2/$5:$3 -o build/containers/$4/$5.tar
    fi 
}

package () {
    stage "Packaging installer"

    makeself \
        --xz \
        --complevel 6 \
        --threads 0 \
        --notemp \
        build portal-install "Merge Portal Installer" ./installer
}

stage () {
    echo -e "$BLUE$1$NORMAL"
}

die () {
    echo -e "$RED$1$NORMAL"
    exit 1
}

RED="\e[1;31m"
BLUE="\e[1;34m"
GREEN="\e[1;32m"
CYAN="\e[1;36m"
NORMAL="\e[39m"

craft
