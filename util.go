package main

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"text/template"
	"unicode/utf8"

	"github.com/cavaliercoder/grab"
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"

	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	v1 "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"

	rbacv1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

func genca() {

	if fileExists(".cert/ca-key.pem") {
		log.Info("Found existing ca cert")
		return
	}

	params := config.Cert.Ca
	params.Ca = config.Domain.Api

	templateExec(caCSR, ".cert/ca-csr.json", params)
	templateExec(caConfig, ".cert/ca-config.json", params)

	log.Info("Generating ca cert")

	cmdpipe(
		exec.Command("../.dep/cfssl", "gencert", "-initca", "ca-csr.json"),
		exec.Command("../.dep/cfssljson", "-bare", "ca"),
		".cert",
	)

}

func gencert(name, domain string, hostnames ...string) {

	if fileExists(".cert/" + name + ".pem") {
		log.Info("Found existing " + name + " cert")
		return
	}

	params := config.Cert.Ca
	params.Ca = domain
	params.Ou = name

	templateExec(caCSR, ".cert/"+name+"-csr.json", params)

	log.Info("Generating " + name + " cert, (hostnames: " + strings.Join(hostnames, ",") + ")")

	cmdpipe(
		exec.Command("../.dep/cfssl", "gencert",
			"-ca=ca.pem",
			"-ca-key=ca-key.pem",
			"-config=ca-config.json",
			"-hostname="+strings.Join(hostnames, ","),
			"-profile="+config.Cert.Ca.Ou,
			name+"-csr.json",
		),
		exec.Command("../.dep/cfssljson", "-bare", name),
		".cert",
	)

}

func bundleca(hostpem, bundle string) {

	hostpath := fmt.Sprintf(".cert/%s", hostpem)
	capath := ".cert/ca.pem"

	part1, err := ioutil.ReadFile(hostpath)
	if err != nil {
		log.Fatalf("read hostpem: %s: %v", hostpath, err)
	}

	part2, err := ioutil.ReadFile(capath)
	if err != nil {
		log.Fatalf("read capem: %s: %v", capath, err)
	}

	out := append(part1, part2...)

	err = ioutil.WriteFile(fmt.Sprintf(".cert/%s", bundle), out, 0644)
	if err != nil {
		log.Fatalf("write bundle: %v", err)
	}

}

func namespace(name string) {

	_, err := config.K8s.client.CoreV1().Namespaces().Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil {
		if force {
			dp := new(metav1.DeletionPropagation)
			*dp = metav1.DeletePropagationForeground
			err := config.K8s.client.CoreV1().Namespaces().Delete(
				context.TODO(),
				name,
				metav1.DeleteOptions{
					//TODO this is still not synchronous, need to wait on
					//namespace delete op
					PropagationPolicy: dp,
				},
			)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			log.Infof("Namespace %s already exists, skipping", name)
			return
		}
	}
	if err != nil && !errors.IsNotFound(err) {
		log.Fatal(err)
	}

	log.Infof("Creating namespace %s", name)

	_, err = config.K8s.client.CoreV1().Namespaces().Create(
		context.TODO(),
		&v1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

func serviceAccount(ns, name string) {

	log.Infof("Creating service account %s/%s", ns, name)

	_, err := config.K8s.client.CoreV1().ServiceAccounts(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil {
		log.Infof("ServiceAccount %s already exists, skipping", name)
		return
	}

	_, err = config.K8s.client.CoreV1().ServiceAccounts(ns).Create(
		context.TODO(),
		&v1.ServiceAccount{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}
func role(name, ns string, rules []rbacv1.PolicyRule) {

	log.Infof("Creating role %s/%s", ns, name)

	_, err := config.K8s.client.RbacV1().Roles(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil {
		log.Infof("role %s/%s already exists, skipping", ns, name)
		return
	}

	_, err = config.K8s.client.RbacV1().Roles(ns).Create(
		context.TODO(),
		&rbacv1.Role{
			ObjectMeta: metav1.ObjectMeta{
				Name:      name,
				Namespace: ns,
			},
			Rules: rules,
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

func roleBinding(name, ns string, subjects []rbacv1.Subject, role rbacv1.RoleRef) {

	log.Infof("Creating role binding %s/%s", ns, name)

	_, err := config.K8s.client.RbacV1().RoleBindings(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil {
		log.Infof("Role binding %s/%s already exists, skipping", ns, name)
		return
	}

	_, err = config.K8s.client.RbacV1().RoleBindings(ns).Create(
		context.TODO(),
		&rbacv1.RoleBinding{
			ObjectMeta: metav1.ObjectMeta{
				Name:      name,
				Namespace: ns,
			},
			Subjects: subjects,
			RoleRef:  role,
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}
}

func clusterRole(name string, rules []rbacv1.PolicyRule) {

	log.Infof("Creating cluster role %s", name)

	_, err := config.K8s.client.RbacV1().ClusterRoles().Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil {
		log.Infof("Cluster role %s already exists, skipping", name)
		return
	}

	_, err = config.K8s.client.RbacV1().ClusterRoles().Create(
		context.TODO(),
		&rbacv1.ClusterRole{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Rules: rules,
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

func clusterRoleBinding(name, namespace string, subjects []rbacv1.Subject, role rbacv1.RoleRef) {

	log.Infof("Creating cluster role binding %s", name)

	_, err := config.K8s.client.RbacV1().ClusterRoleBindings().Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil {
		log.Infof("Cluster role binding %s already exists, skipping", name)
		return
	}

	_, err = config.K8s.client.RbacV1().ClusterRoleBindings().Create(
		context.TODO(),
		&rbacv1.ClusterRoleBinding{
			ObjectMeta: metav1.ObjectMeta{
				Name:      name,
				Namespace: namespace,
			},
			Subjects: subjects,
			RoleRef:  role,
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

func certCM(ns, svcname string) {

	fileCM(ns, svcname,
		".cert/ca.pem",
		fmt.Sprintf(".cert/%s.pem", svcname),
		fmt.Sprintf(".cert/%s-key.pem", svcname),
	)

}

func mapCM(ns, name string, d map[string]string, b map[string][]byte) {

	_, err := config.K8s.client.CoreV1().ConfigMaps(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil {
		log.Infof("Configmap for %s already exists, skipping", name)
		return
	}

	if err != nil && !errors.IsNotFound(err) {
		log.Fatal(err)
	}

	log.Infof("Creating configmap for %s", name)

	_, err = config.K8s.client.CoreV1().ConfigMaps(ns).Create(
		context.TODO(),
		&v1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Data:       d,
			BinaryData: b,
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

func fileCM(ns, name string, files ...string) {

	cm, err := config.K8s.client.CoreV1().ConfigMaps(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil && !force {
		log.Infof("Configmap for %s already exists, skipping", name)
		return
	}

	if err != nil && !errors.IsNotFound(err) {
		log.Fatal(err)
	}

	log.Infof("Creating configmap for %s", name)

	cm = &v1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
	}

	for _, filename := range files {
		buf, err := ioutil.ReadFile(filename)
		if err != nil {
			log.Fatal(err)
		}
		if utf8.Valid(buf) {
			if cm.Data == nil {
				cm.Data = make(map[string]string)
			}
			cm.Data[filepath.Base(filename)] = string(buf)
		} else {
			if cm.BinaryData == nil {
				cm.BinaryData = make(map[string][]byte)
			}
			cm.BinaryData[filepath.Base(filename)] = buf
		}
	}

	_, err = config.K8s.client.CoreV1().ConfigMaps(ns).Create(
		context.TODO(), cm, metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

func fileSecrets(ns, name string, files map[string]string) {

	secret, err := config.K8s.client.CoreV1().Secrets(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil && !force {
		log.Infof("Secret for %s/%s already exists, skipping", ns, name)
		return
	}

	if err != nil && !errors.IsNotFound(err) {
		log.Fatal(err)
	}

	log.Infof("Creating secret for %s/%s", ns, name)

	secret = &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
		Type: v1.SecretTypeTLS,
	}

	for key, filename := range files {
		buf, err := ioutil.ReadFile(filename)
		if err != nil {
			log.Fatal(err)
		}
		if utf8.Valid(buf) {
			if secret.Data == nil {
				secret.Data = make(map[string][]byte)
			}
			secret.Data[key] = buf
		}
	}

	_, err = config.K8s.client.CoreV1().Secrets(ns).Create(
		context.TODO(), secret, metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

func basicWorker(name string, env []v1.EnvVar, cmd []string) {

	deployment(
		mergeNs,
		name,
		img(mergeNs, name),
		nil,
		[]v1.VolumeMount{
			{Name: "db-cert-volume", MountPath: "/dbcerts"},
		},
		[]v1.Volume{{
			Name: "db-cert-volume",
			VolumeSource: v1.VolumeSource{
				ConfigMap: &v1.ConfigMapVolumeSource{
					LocalObjectReference: v1.LocalObjectReference{
						Name: "etcd",
					},
				},
			},
		}},
		env,
		cmd,
	)

}

func basicService(ns, name string, port int32, env []v1.EnvVar) {

	service(ns, name, "portal", []v1.ServicePort{{
		Protocol:   "TCP",
		Port:       port,
		TargetPort: intstr.FromInt(int(port)),
	}})

	deployment(
		ns,
		name,
		img(ns, name),
		[]v1.ContainerPort{
			{Name: "grpc", ContainerPort: port},
		},
		[]v1.VolumeMount{
			{Name: "db-cert-volume", MountPath: "/dbcerts"},
		},
		[]v1.Volume{{
			Name: "db-cert-volume",
			VolumeSource: v1.VolumeSource{
				ConfigMap: &v1.ConfigMapVolumeSource{
					LocalObjectReference: v1.LocalObjectReference{
						Name: "etcd",
					},
				},
			},
		}},
		env,
		nil,
	)

}

func xdcSshJump() {

	ns, name, app := xdcNs, "ssh-jump", "portal"
	var port int32 = 2022
	var xport int32 = 6000 // xdcd listens here
	replicas := new(int32)
	*replicas = 1

	var priv bool = true

	_, err := config.K8s.client.CoreV1().Services(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil {
		log.Infof("Service %s already exists, skipping", name)
	} else {

		log.Infof("Creating %s service", name)

		_, err = config.K8s.client.CoreV1().Services(ns).Create(
			context.TODO(),
			&v1.Service{
				ObjectMeta: metav1.ObjectMeta{
					Name: name,
					Labels: map[string]string{
						"app":       app,
						"component": "ssh-jump",
						"fqdn":      "jump." + config.Domain.Xdc,
					},
				},
				Spec: v1.ServiceSpec{
					Type: v1.ServiceTypeNodePort,
					Selector: map[string]string{
						"app":       app,
						"component": "xdc",
						"instance":  "jump",
					},
					Ports: []v1.ServicePort{{
						Name:     "ssh",
						Protocol: v1.ProtocolTCP,
						Port:     port,
					}, {
						Name:     "xdcd",
						Protocol: v1.ProtocolTCP,
						Port:     xport,
					}},
					ExternalIPs: []string{
						config.Domain.JumpAddr,
					},
				},
			},
			metav1.CreateOptions{},
		)
		if err != nil {
			log.Fatal(err)
		}
	}

	deploy(
		ns, name,
		&appsv1.Deployment{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Deployment",
				APIVersion: "apps/v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Spec: appsv1.DeploymentSpec{
				Replicas: replicas,
				Selector: &metav1.LabelSelector{
					MatchLabels: map[string]string{
						"app":       app,
						"component": "xdc",
						"instance":  "jump",
						"svc":       name,
						"fqdn":      "jump." + config.Domain.Xdc,
					},
				},
				Template: v1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app":       app,
							"component": "xdc",
							"instance":  "jump",
							"svc":       name,
							"fqdn":      "jump." + config.Domain.Xdc,
						},
					},
					Spec: v1.PodSpec{
						Tolerations: []v1.Toleration{{
							Key:      "dedicated",
							Operator: v1.TolerationOpEqual,
							Value:    "service_worker",
							Effect:   v1.TaintEffectNoSchedule,
						}},
						Containers: []v1.Container{{
							Name:            name,
							Image:           img(ns, name),
							ImagePullPolicy: config.GetImagePullPolicy(),
							SecurityContext: &v1.SecurityContext{
								Privileged: &priv,
								Capabilities: &v1.Capabilities{
									Add: []v1.Capability{"SYS_CHROOT"},
								},
							},
							Ports: []v1.ContainerPort{
								{Name: "ssh", ContainerPort: port},
								{Name: "xdcd", ContainerPort: xport},
							},
							VolumeMounts: []v1.VolumeMount{{
								Name:      "mergefs-volume",
								MountPath: "/home",
								SubPath:   "user/",
								ReadOnly:  true,
							}, {
								Name:      "mergefs-volume",
								MountPath: "/etc/ssh/auth",
								SubPath:   "xdc/" + name + "/auth",
							}, {
								Name:      "step-ca-volume",
								MountPath: "/etc/step-ca/data/certs",
								SubPath:   "certs",
								ReadOnly:  true,
							}},
						}},
						Volumes: []v1.Volume{{
							Name: "mergefs-volume",
							VolumeSource: v1.VolumeSource{
								PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
									ClaimName: "pvc-mergefs-xdc",
								},
							},
						}, {
							Name: "step-ca-volume",
							VolumeSource: v1.VolumeSource{
								PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
									ClaimName: "pvc-step-ca-xdc",
								},
							},
						}},
					},
				},
			},
		})
}

func xdcService() {

	ns, name, app := mergeNs, "xdc", "portal"
	var port int32 = 6000

	service(ns, name, app, []v1.ServicePort{{
		Protocol:   v1.ProtocolTCP,
		Port:       port,
		TargetPort: intstr.FromInt(int(port)),
	}})

	replicas := new(int32)
	*replicas = 1

	deploy(
		ns, name,
		&appsv1.Deployment{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Deployment",
				APIVersion: "apps/v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Spec: appsv1.DeploymentSpec{
				Replicas: replicas, //TODO
				Selector: &metav1.LabelSelector{
					MatchLabels: map[string]string{
						"app":       app,
						"component": name,
					},
				},
				Template: v1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app":       app,
							"component": name,
						},
					},
					Spec: v1.PodSpec{
						ServiceAccountName: "xdc-controller",
						Tolerations: []v1.Toleration{
							{
								Key:      "dedicated",
								Operator: v1.TolerationOpEqual,
								Value:    "service_worker",
								Effect:   v1.TaintEffectNoSchedule,
							},
						},
						Containers: []v1.Container{{
							Name:            name,
							Image:           img(ns, name),
							ImagePullPolicy: config.GetImagePullPolicy(),
							SecurityContext: &v1.SecurityContext{
								Capabilities: &v1.Capabilities{
									Add: []v1.Capability{"SYS_CHROOT"},
								},
							},
							Ports: []v1.ContainerPort{
								{Name: "grpc", ContainerPort: port},
							},
							VolumeMounts: []v1.VolumeMount{
								{Name: "db-cert-volume", MountPath: "/dbcerts"},
							},
							Env: []v1.EnvVar{
								{Name: "XDC_DOMAIN", Value: "xdc." + config.Domain.Xdc},
								{Name: "XDC_NAMESPACE", Value: xdcNs},
								{Name: "XDC_REGISTRY", Value: config.RemoteRegistry.Registry.Internal},
								{Name: "XDC_IMAGE", Value: img(xdcNs, "xdc-base")},
							},
						}},
						Volumes: []v1.Volume{{
							Name: "db-cert-volume",
							VolumeSource: v1.VolumeSource{
								ConfigMap: &v1.ConfigMapVolumeSource{
									LocalObjectReference: v1.LocalObjectReference{
										Name: "etcd",
									},
								},
							},
						}},
					},
				},
			},
		},
	)
}

func mergefsService() {

	pvc(mergeNs, "mergefs", config.K8s.PersistentVolumes.Mergefs, "100Gi")

	ns, name, app := mergeNs, "mergefs", "portal"
	var port int32 = 6000

	service(ns, name, app, []v1.ServicePort{{
		Protocol:   "TCP",
		Port:       port,
		TargetPort: intstr.FromInt(int(port)),
	}})

	deployment(
		ns,
		name,
		img(ns, name),
		[]v1.ContainerPort{
			{Name: "grpc", ContainerPort: port},
		},
		[]v1.VolumeMount{
			{Name: "db-cert-volume", MountPath: "/dbcerts"},
			{Name: "mergefs-volume", MountPath: "/mergefs"},
			{
				// mergefs needs to read the CA Host pub key to write to users' known_hosts file.
				Name:      "step-ca-volume",
				MountPath: "/etc/step-ca/data/certs",
				SubPath:   "certs",
				ReadOnly:  true,
			},
		},
		[]v1.Volume{{
			Name: "db-cert-volume",
			VolumeSource: v1.VolumeSource{
				ConfigMap: &v1.ConfigMapVolumeSource{
					LocalObjectReference: v1.LocalObjectReference{
						Name: "etcd",
					},
				},
			},
		}, {
			Name: "mergefs-volume",
			VolumeSource: v1.VolumeSource{
				PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
					ClaimName: "mergefs",
				},
			},
		}, {
			Name: "step-ca-volume",
			VolumeSource: v1.VolumeSource{
				PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
					ClaimName: "step-ca",
				},
			},
		}},
		nil,
		[]string{
			"/usr/bin/mergefs",
		},
	)
}

func gitServer() {

	pvc(mergeNs, "git", config.K8s.PersistentVolumes.Gitfs, "5Gi")

	service(mergeNs, "git-server", "portal", []v1.ServicePort{{
		Protocol:   "TCP",
		Port:       8080,
		TargetPort: intstr.FromInt(8080),
	}})

	replicas := new(int32)
	*replicas = 1

	deploy(
		mergeNs,
		"git-server",
		&appsv1.Deployment{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Deployment",
				APIVersion: "apps/v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: "git-server",
			},
			Spec: appsv1.DeploymentSpec{
				Replicas: replicas, //TODO
				Selector: &metav1.LabelSelector{
					MatchLabels: map[string]string{
						"app":       "portal",
						"component": "git-server",
					},
				},
				Template: v1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app":       "portal",
							"component": "git-server",
						},
					},
					Spec: v1.PodSpec{
						Tolerations: []v1.Toleration{
							{
								Key:      "dedicated",
								Operator: v1.TolerationOpEqual,
								Value:    "service_worker",
								Effect:   v1.TaintEffectNoSchedule,
							},
						},
						Containers: []v1.Container{
							{
								Name:            "git-server",
								Image:           img(mergeNs, "git-server"),
								ImagePullPolicy: config.GetImagePullPolicy(),
								Ports: []v1.ContainerPort{
									{Name: "public", ContainerPort: 5432},
								},
								VolumeMounts: []v1.VolumeMount{
									{Name: "db-cert-volume", MountPath: "/dbcerts"},
									{Name: "data-volume", MountPath: "/var/git"},
									{Name: "policy-volume", MountPath: "/etc/merge/policy.yml", SubPath: "policy.yml"},
								},
								Env: []v1.EnvVar{
									{Name: "AUTH_NAMESPACE", Value: config.K8s.Namespace.Auth},
								},
							},
						},
						Volumes: []v1.Volume{{
							Name: "db-cert-volume",
							VolumeSource: v1.VolumeSource{
								ConfigMap: &v1.ConfigMapVolumeSource{
									LocalObjectReference: v1.LocalObjectReference{
										Name: "etcd",
									},
								},
							},
						}, {
							Name: "data-volume",
							VolumeSource: v1.VolumeSource{
								PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
									ClaimName: "git",
								},
							},
						}, {
							Name: "policy-volume",
							VolumeSource: v1.VolumeSource{
								ConfigMap: &v1.ConfigMapVolumeSource{
									LocalObjectReference: v1.LocalObjectReference{
										Name: "policy",
									},
								},
							},
						}},
					},
				},
			},
		},
	)

}

func apiService(name string) {

	service(mergeNs, name, "portal", []v1.ServicePort{
		{
			Protocol:   "TCP",
			Port:       6000,
			TargetPort: intstr.FromInt(int(6000)),
			Name:       "grpc",
		},
		{
			Protocol:   "TCP",
			Port:       8081,
			TargetPort: intstr.FromInt(int(8081)),
			Name:       "rest",
		},
	})

	deployment(
		mergeNs,
		name,
		img(mergeNs, name),
		[]v1.ContainerPort{
			{Name: "grpc", ContainerPort: 6000},
			{Name: "rest", ContainerPort: 8081},
		},
		[]v1.VolumeMount{
			{
				Name:      "db-cert-volume",
				MountPath: "/dbcerts",
			},
			{
				Name:      "apiserver-cert-volume",
				MountPath: "/certs",
			},
			{
				Name:      "policy-volume",
				MountPath: "/etc/merge/policy.yml",
				SubPath:   "policy.yml",
			},
		},
		[]v1.Volume{
			{
				Name: "db-cert-volume",
				VolumeSource: v1.VolumeSource{
					ConfigMap: &v1.ConfigMapVolumeSource{
						LocalObjectReference: v1.LocalObjectReference{
							Name: "etcd",
						},
					},
				},
			},
			{
				Name: "apiserver-cert-volume",
				VolumeSource: v1.VolumeSource{
					ConfigMap: &v1.ConfigMapVolumeSource{
						LocalObjectReference: v1.LocalObjectReference{
							Name: "apiserver",
						},
					},
				},
			},
			{
				Name: "policy-volume",
				VolumeSource: v1.VolumeSource{
					ConfigMap: &v1.ConfigMapVolumeSource{
						LocalObjectReference: v1.LocalObjectReference{
							Name: "policy",
						},
					},
				},
			},
		},
		[]v1.EnvVar{
			{Name: "MINIO_ACCESS_KEY", Value: gconf.Minio.Access},
			{Name: "MINIO_SECRET_KEY", Value: gconf.Minio.Secret},
			{Name: "AUTH_NAMESPACE", Value: config.K8s.Namespace.Auth},
		},
		[]string{ // Pass relavant configuration into the apiserver.
			"/usr/bin/apiserver",
			"-api", "api." + config.Domain.Api + ":443",
			"-grpc", "grpc." + config.Domain.Api + ":443",
			"-origins", "https://*." + config.Domain.Api,
		},
	)
}

func job(ns, name, image string, cmd ...string) {

	_, err := config.K8s.client.BatchV1().Jobs(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil {
		if !force && !redeploy {
			log.Infof("Job %s exists, skipping", name)
			return
		}

		log.Infof("Job %s exists, deleting and redeploying", name)

		propagation := new(metav1.DeletionPropagation)
		*propagation = metav1.DeletePropagationBackground

		err = config.K8s.client.BatchV1().Jobs(ns).Delete(context.TODO(),
			name,
			metav1.DeleteOptions{
				PropagationPolicy: propagation,
			},
		)
		if err != nil {
			log.Fatal(fmt.Errorf("failed to delete job: %w", err))
		}
	}

	retries := int32(30)

	log.Infof("Creating %s/%s job", ns, name)

	_, err = config.K8s.client.BatchV1().Jobs(ns).Create(
		context.TODO(),
		&batchv1.Job{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Spec: batchv1.JobSpec{
				BackoffLimit: &retries,
				Template: v1.PodTemplateSpec{
					Spec: v1.PodSpec{
						Containers: []v1.Container{{
							Name:            name,
							Image:           image,
							ImagePullPolicy: config.GetImagePullPolicy(),
							Command:         cmd,
						}},
						RestartPolicy: v1.RestartPolicyNever,
					},
				},
			},
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

func ingress(ns, name, domain, service string, port int) {

	_, err := config.K8s.client.NetworkingV1().Ingresses(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil && !force {
		log.Infof("Ingress %s/%s already exists, skipping", ns, name)
		return
	}

	log.Infof("Creating %s/%s ingress", ns, name)

	_, err = config.K8s.client.NetworkingV1().Ingresses(ns).Create(
		context.TODO(),
		&netv1.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Spec: netv1.IngressSpec{
				Rules: []netv1.IngressRule{
					{
						Host: domain,
						IngressRuleValue: netv1.IngressRuleValue{
							HTTP: &netv1.HTTPIngressRuleValue{
								Paths: []netv1.HTTPIngressPath{{
									Path:     "/",
									PathType: &prefixPathType,
									Backend: netv1.IngressBackend{
										Service: &netv1.IngressServiceBackend{
											Name: service,
											Port: netv1.ServiceBackendPort{
												Number: int32(port),
											},
										},
									},
								}},
							},
						},
					},
				},
			},
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

func ingressTLSTerminate(ns, name, domain, service, key string, port int) {

	_, err := config.K8s.client.NetworkingV1().Ingresses(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil && !force {
		log.Infof("Ingress %s/%s already exists, skipping", ns, name)
		return
	}

	log.Infof("Creating %s/%s ingress", ns, name)

	_, err = config.K8s.client.NetworkingV1().Ingresses(ns).Create(
		context.TODO(),
		&netv1.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Spec: netv1.IngressSpec{
				TLS: []netv1.IngressTLS{
					{
						Hosts:      []string{domain},
						SecretName: key,
					},
				},
				Rules: []netv1.IngressRule{
					{
						Host: domain,
						IngressRuleValue: netv1.IngressRuleValue{
							HTTP: &netv1.HTTPIngressRuleValue{
								Paths: []netv1.HTTPIngressPath{{
									Path:     "/",
									PathType: &prefixPathType,
									Backend: netv1.IngressBackend{
										Service: &netv1.IngressServiceBackend{
											Name: service,
											Port: netv1.ServiceBackendPort{
												Number: int32(port),
											},
										},
									},
								}},
							},
						},
					},
				},
			},
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

var prefixPathType = netv1.PathType("Prefix")

func openshiftIngressTLSReencrypt(ns, name, domain, service, key string, port int) {

	_, err := config.K8s.client.NetworkingV1().Ingresses(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil && !force {
		log.Infof("Ingress %s/%s already exists, skipping", ns, name)
		return
	}

	cacert, err := ioutil.ReadFile(".cert/ca.pem")
	if err != nil {
		log.Fatalf("read ca cert: %v", err)
	}

	log.Infof("Creating %s/%s ingress", ns, name)

	_, err = config.K8s.client.NetworkingV1().Ingresses(ns).Create(
		context.TODO(),
		&netv1.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
				Annotations: map[string]string{
					"route.openshift.io/termination":              "reencrypt",
					"route.openshift.io/destinationCACertificate": string(cacert),
				},
			},
			Spec: netv1.IngressSpec{
				TLS: []netv1.IngressTLS{
					{
						Hosts:      []string{domain},
						SecretName: key,
					},
				},
				Rules: []netv1.IngressRule{
					{
						Host: domain,
						IngressRuleValue: netv1.IngressRuleValue{
							HTTP: &netv1.HTTPIngressRuleValue{
								Paths: []netv1.HTTPIngressPath{{
									Path:     "/",
									PathType: &prefixPathType,
									Backend: netv1.IngressBackend{
										Service: &netv1.IngressServiceBackend{
											Name: service,
											Port: netv1.ServiceBackendPort{
												Number: int32(port),
											},
										},
									},
								}},
							},
						},
					},
				},
			},
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

func nginxIngressTLSPassthrough(ns, name, domain, service, key string, port int) {

	_, err := config.K8s.client.NetworkingV1().Ingresses(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil && !force {
		log.Infof("Ingress %s/%s already exists, skipping", ns, name)
		return
	}

	log.Infof("Creating %s/%s ingress", ns, name)

	_, err = config.K8s.client.NetworkingV1().Ingresses(ns).Create(
		context.TODO(),
		&netv1.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
				Annotations: map[string]string{
					"nginx.ingress.kubernetes.io/ssl-passthrough": "true",
				},
			},
			Spec: netv1.IngressSpec{
				TLS: []netv1.IngressTLS{
					{
						Hosts:      []string{domain},
						SecretName: key,
					},
				},
				Rules: []netv1.IngressRule{
					{
						Host: domain,
						IngressRuleValue: netv1.IngressRuleValue{
							HTTP: &netv1.HTTPIngressRuleValue{
								Paths: []netv1.HTTPIngressPath{{
									Path:     "/",
									PathType: &prefixPathType,
									Backend: netv1.IngressBackend{
										Service: &netv1.IngressServiceBackend{
											Name: service,
											Port: netv1.ServiceBackendPort{
												Number: int32(port),
											},
										},
									},
								}},
							},
						},
					},
				},
			},
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

func openshiftMultipathIngress(ns, name, domain, key, rewrite string, paths []netv1.HTTPIngressPath) {

	_, err := config.K8s.client.NetworkingV1().Ingresses(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil && !force {
		log.Infof("Ingress %s/%s already exists, skipping", ns, name)
		return
	}

	log.Infof("Creating %s/%s ingress", ns, name)

	_, err = config.K8s.client.NetworkingV1().Ingresses(ns).Create(
		context.TODO(),
		&netv1.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
				Annotations: map[string]string{
					"haproxy.router.openshift.io/rewrite-target": rewrite,
				},
			},
			Spec: netv1.IngressSpec{
				TLS: []netv1.IngressTLS{
					{
						Hosts:      []string{domain},
						SecretName: key,
					},
				},
				Rules: []netv1.IngressRule{
					{
						Host: domain,
						IngressRuleValue: netv1.IngressRuleValue{
							HTTP: &netv1.HTTPIngressRuleValue{
								Paths: paths,
							},
						},
					},
				},
			},
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

func nginxMultipathIngress(ns, name, domain, key, rewrite string, paths []netv1.HTTPIngressPath) {

	_, err := config.K8s.client.NetworkingV1().Ingresses(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil && !force {
		log.Infof("Ingress %s/%s already exists, skipping", ns, name)
		return
	}

	log.Infof("Creating %s/%s ingress", ns, name)

	_, err = config.K8s.client.NetworkingV1().Ingresses(ns).Create(
		context.TODO(),
		&netv1.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
				Annotations: map[string]string{
					"nginx.ingress.kubernetes.io/rewrite-target": rewrite,
				},
			},
			Spec: netv1.IngressSpec{
				TLS: []netv1.IngressTLS{
					{
						Hosts:      []string{domain},
						SecretName: key,
					},
				},
				Rules: []netv1.IngressRule{
					{
						Host: domain,
						IngressRuleValue: netv1.IngressRuleValue{
							HTTP: &netv1.HTTPIngressRuleValue{
								Paths: paths,
							},
						},
					},
				},
			},
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

func duppv(newname, origname string) {

	origpv, err := config.K8s.client.CoreV1().PersistentVolumes().Get(
		context.TODO(),
		origname,
		metav1.GetOptions{},
	)
	if err != nil {
		log.Infof("Get PersistentVolume error: %+v", err)
		return
	}

	// confirm RWX in the PV
	ok := false
	for _, am := range origpv.Spec.AccessModes {
		if am == v1.ReadWriteMany {
			ok = true
			break
		}
	}
	if !ok {
		log.Fatalf("pvc %s must support RWX access", origname)
	}

	_, err = config.K8s.client.CoreV1().PersistentVolumes().Get(
		context.TODO(),
		newname,
		metav1.GetOptions{},
	)
	if err == nil && !force {
		log.Infof("Persistent volume claim for %s already exists, skipping", newname)
		return
	}

	log.Infof("Duplicating persistent volume %s as %s", origname, newname)

	// if the PV we are duplicating is already bound, do not copy that!
	if origpv.Spec.ClaimRef != nil {
		origpv.Spec.ClaimRef = nil
	}

	// Create the new PV from teh orig PV spec.
	_, err = config.K8s.client.CoreV1().PersistentVolumes().Create(
		context.TODO(),
		&v1.PersistentVolume{
			ObjectMeta: metav1.ObjectMeta{
				Name: newname,
				Labels: map[string]string{
					"volume": newname,
				},
			},
			Spec: origpv.Spec,
		},
		metav1.CreateOptions{},
	)
}

func pvc(ns, name, volume, size string) {

	_, err := config.K8s.client.CoreV1().PersistentVolumeClaims(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil && !force {
		log.Infof("Persistent volume claim for %s already exists, skipping", name)
		return
	}

	log.Infof("Creating %s persistent volume claim", name)

	_, err = config.K8s.client.CoreV1().PersistentVolumeClaims(ns).Create(
		context.TODO(),
		&v1.PersistentVolumeClaim{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Spec: v1.PersistentVolumeClaimSpec{
				AccessModes: []v1.PersistentVolumeAccessMode{
					v1.ReadWriteMany,
				},
				Resources: v1.ResourceRequirements{
					Requests: v1.ResourceList{
						v1.ResourceStorage: resource.MustParse(size),
					},
				},
				VolumeName: volume,
			},
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

func service(ns, name, app string, ports []v1.ServicePort) {

	_, err := config.K8s.client.CoreV1().Services(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil {
		log.Infof("Service %s already exists, skipping", name)
		return
	}

	log.Infof("Creating %s service", name)

	_, err = config.K8s.client.CoreV1().Services(ns).Create(
		context.TODO(),
		&v1.Service{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Spec: v1.ServiceSpec{
				Selector: map[string]string{
					"app":       app,
					"component": name,
				},
				Ports: ports,
			},
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		log.Fatal(err)
	}

}

func deploy(ns, name string, d *appsv1.Deployment) {
	_, err := config.K8s.client.AppsV1().Deployments(ns).Get(
		context.TODO(),
		name,
		metav1.GetOptions{},
	)
	if err == nil {
		if !force && !redeploy {
			log.Infof("Deployment %s exists, skipping", name)
			return
		}

		log.Infof("Deployment %s exists, deleting and redeploying", name)

		propagation := new(metav1.DeletionPropagation)
		*propagation = metav1.DeletePropagationBackground

		err = config.K8s.client.AppsV1().Deployments(ns).Delete(context.TODO(),
			name,
			metav1.DeleteOptions{
				PropagationPolicy: propagation,
			},
		)
		if err != nil {
			log.Fatal(fmt.Errorf("failed to delete deployment: %w", err))
		}
	}

	log.Infof("Creating %s deployment", name)

	replicas := new(int32)
	*replicas = 1

	_, err = config.K8s.client.AppsV1().Deployments(ns).Create(
		context.TODO(),
		d,
		metav1.CreateOptions{},
	)

	if err != nil {
		log.Fatal(err)
	}

}

func deployment(
	ns, name, image string,
	ports []v1.ContainerPort,
	mounts []v1.VolumeMount,
	volumes []v1.Volume,
	env []v1.EnvVar,
	cmd []string,
) {

	replicas := new(int32)
	*replicas = 1

	deploy(
		ns, name,
		&appsv1.Deployment{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Deployment",
				APIVersion: "apps/v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
			Spec: appsv1.DeploymentSpec{
				Replicas: replicas, //TODO
				Selector: &metav1.LabelSelector{
					MatchLabels: map[string]string{
						"app":       "portal",
						"component": name,
					},
				},
				Template: v1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app":       "portal",
							"component": name,
						},
					},
					Spec: v1.PodSpec{
						Tolerations: []v1.Toleration{
							{
								Key:      "dedicated",
								Operator: v1.TolerationOpEqual,
								Value:    "service_worker",
								Effect:   v1.TaintEffectNoSchedule,
							},
						},
						Containers: []v1.Container{
							{
								Name:            name,
								Image:           image,
								ImagePullPolicy: config.GetImagePullPolicy(),
								Ports:           ports,
								VolumeMounts:    mounts,
								Command:         cmd,
								Env:             env,
							},
						},
						Volumes: volumes,
					},
				},
			},
		},
	)

}

func cmdpipe(c1, c2 *exec.Cmd, dir string) {

	r, w := io.Pipe()
	c1.Dir = dir
	c2.Dir = dir
	c1.Stdout = w
	c2.Stdin = r

	var out bytes.Buffer
	c2.Stderr = &out

	c1.Start()
	c2.Start()
	err := c1.Wait()
	if err != nil {
		log.Error(err)
	}
	w.Close()
	err = c2.Wait()
	if err != nil {
		log.Error(err)
		log.Fatal(out.String())
	}

}

func templateExec(src, dest string, data interface{}) {

	tmpl, err := template.New("builtin").Parse(src)
	if err != nil {
		log.Fatal(err)
	}

	f, err := os.Create(dest)
	if err != nil {
		log.Fatal(err)
	}

	err = tmpl.Execute(f, data)
	if err != nil {
		log.Fatal(err)
	}

}

func templateString(src string, data interface{}) string {

	tmpl, err := template.New("builtin").Parse(src)
	if err != nil {
		log.Fatal(err)
	}

	var buf bytes.Buffer
	err = tmpl.Execute(&buf, data)
	if err != nil {
		log.Fatal(err)
	}

	return buf.String()
}

func require(what, url string) {

	filename := ".dep/" + what
	if fileExists(filename) {
		return
	}

	err := os.MkdirAll(".dep", 0755)
	if err != nil {
		log.Fatal(err)
	}

	log.Infof("fetching %s", what)

	client := grab.NewClient()
	req, err := grab.NewRequest("", url)
	if err != nil {
		log.Fatal(err)
	}
	req.Filename = filename
	resp := client.Do(req)

	for {
		select {
		case <-resp.Done:
			if err := resp.Err(); err != nil {
				log.Fatal(err)
			}
			os.Chmod(filename, 0755)
			return
		}
	}

}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func fqdn(name, domain string) string { return name + "." + domain }

func ydump(x interface{}, fmt string, f func(string, ...interface{})) {

	out, err := yaml.Marshal(x)
	if err != nil {
		log.Fatal(err)
	}

	f(fmt, string(out))

}

func img(ns, name string) string {

	image := config.GetImageOverride(name)

	if image != "" {
		return image
	}

	return fmt.Sprintf(
		"%s/%s/%s:%s",
		config.RemoteRegistry.Registry.Internal,
		ns,
		name,
		config.RemoteRegistry.Registry.Tag,
	)

}
